;
window.onload = function () {
    layui.use(['layer'], function () {
        var layer = layui.layer;

        // ----------------登录表单----------------
        var oLoginForm = document.getElementById('oLoginForm');
        var oLoginUser = document.getElementById('oLoginUser');
        var oLoginPasswd = document.getElementById('oLoginPasswd');
        var oSaveMe = document.getElementById('oSaveMe');

        //表单是否可以提交
        var bLoginUser = true;
        var bLoginPass = true;

        //最近一次登录的用户
        var sLastLogin = 'lastLogin';

        //“30天内记住我”功能
        if (getCookie(sLastLogin) != '') {  //上次有用户登录
            if (getCookie(getCookie(sLastLogin)) != '') {  //上次登录的用户勾选了“30天内记住我”
                oLoginUser.style.backgroundColor = '#faffbd';
                oLoginPasswd.style.backgroundColor = '#faffbd';
                oLoginUser.value = getCookie(sLastLogin);
                oLoginPasswd.value = getCookie(getCookie(sLastLogin));
            } else {  //上次登录的用户未勾选“30天内记住我”
                oLoginUser.style.backgroundColor = '#faffbd';
                oLoginPasswd.style.backgroundColor = '#fff';
                oLoginUser.value = getCookie(sLastLogin);
                oLoginPasswd.value = '';
            }
        }

        var reLoginUser = /^(?:[a-zA-Z]{4,16}|[0-9]{9,16}|(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z]{4,16})$/;
        //ajax验证用户输入的用户名是否存在
        oLoginUser.oninput = function () {
            var _thisValue = this.value.replace(/\s/g, "");  //移除所有空格

            if (_thisValue == '') {  //输入的值为空，清空输入框
                oLoginPasswd.value = '';
                this.style.backgroundColor = '#fff';
                oLoginPasswd.style.backgroundColor = '#fff';
                this.className = 'form-input login-input';
                oLoginPasswd.className = 'form-input login-input';
            } else {
                if (_thisValue.match(reLoginUser)) {  //匹配到用户输入的是用户名而不是邮箱
                    ajax('./admin/data.test.php', '?userName=' + _thisValue, fnSuccLoginUserEmail, fnFailLoginUserEmail);
                } else {
                    ajax('./admin/data.test.php', '?email=' + _thisValue, fnSuccLoginUserEmail, fnFailLoginUserEmail);
                }
            }

            //勾选“30”天记住我，输入用户名后，自动补全密码
            if (getCookie(this.value)) {
                oLoginPasswd.value = getCookie(this.value);
                oLoginPasswd.style.backgroundColor = '#faffbd';
            } else {
                oLoginPasswd.style.backgroundColor = '#fff';
            }
        }

        //ajax检测用户或邮箱成功时的回调函数
        function fnSuccLoginUserEmail() {
            oLoginUser.style.backgroundColor = '#faffbd';
            oLoginUser.className = 'form-input login-input right';
            bLoginUser = true;
        }
        //ajax检测用户或邮箱失败时的回调函数
        function fnFailLoginUserEmail() {
            oLoginUser.style.backgroundColor = '#fff';
            oLoginUser.className = 'form-input login-input error';
            oLoginPasswd.value = '';
            oLoginPasswd.style.backgroundColor = '#fff';
            bLoginUser = false;
        }

        // 密码框输入
        oLoginPasswd.oninput = function () {
            var _thisValue = this.value.replace(/\s/g, "");  //移除所有空格

            if (_thisValue == '') {
                this.style.backgroundColor = '#fff';
            } else {
                this.style.backgroundColor = '#faffbd';
                this.className = 'form-input login-input';
                
                ajax('./admin/data.test.php', '?password=' + _thisValue, fnSuccLoginUserPass, fnFailLoginUserPass);
            }
        }

        function fnSuccLoginUserPass() {
            bLoginPass = true;
        }
        function fnFailLoginUserPass() {
            bLoginPass = false;
        }

        // 用户名输入框失去焦点    
        oLoginUser.onblur = function () {
            if (this.value == '') {
                this.className = 'form-input login-input error';
            } else {
                this.className = 'form-input login-input';
            }
        }
        // 密码输入框失去焦点
        oLoginPasswd.onblur = function () {
            if (this.value == '') {
                this.className = 'form-input login-input error';
            } else {
                this.className = 'form-input login-input';
            }
        }

        //登录表单提交
        oLoginForm.onsubmit = function () {
            var _UserValue = oLoginUser.value.replace(/\s/g, "");  //移除所有空格
            var _PassValue = oLoginPasswd.value.replace(/\s/g, "");  //移除所有空格
            
            // 储存最近一次登录的用户
            removeCookie(sLastLogin);  //先清除原来储存的最近一次登录的用户的用户名
            setCookie(sLastLogin, _UserValue, 30);

            //“30天内记住我”被选中
            if (oSaveMe.checked == true) {
                setCookie(_UserValue, _PassValue, 30);
            } else {
                //未选中“30天内记住我”按钮，删除这个用户的cookie
                removeCookie(_UserValue);
            }

            if (bLoginUser == false) {
                alertConfirm('用户名不存在');
                
                return false;
            }

            if (bLoginPass == false) {
                alertConfirm('密码错误');

                return false;
            }
        }

        // ----------------注册表单----------------
        var oRegisterForm = document.getElementById('oRegisterForm');
        var oRegisterUser = document.getElementById('oRegisterUser');
        var oRegisterEmail = document.getElementById('oRegisterEmail');
        var oRegisterPasswd = document.getElementById('oRegisterPasswd');
        var oRegisterPasswdAgain = document.getElementById('oRegisterPasswdAgain');
        var aRegisterFormInputs = oRegisterForm.getElementsByTagName('input');

        var oRegUserLabel = document.getElementById('oRegUserLabel');
        var oRegPassLabel = document.getElementById('oRegPassLabel');

        var oUserExist = document.getElementById('oUserExist');
        var oEmailExist = document.getElementById('oEmailExist');

        //表单是否可以提交
        var bRegiSubmitUser = false;
        var bRegiSubmitEmail = false;
        var bRegiSubmitPass = false;

        //注册时，点击用户名、密码输入框，提示相应的要求信息
        oRegisterUser.onfocus = function () {
            oRegUserLabel.className = 'userPromptMess';
        }
        oRegisterPasswd.onfocus = function () {
            oRegPassLabel.className = 'passPromptMess';
        }

        for (let i = 0; i < aRegisterFormInputs.length; i++) {
            aRegisterFormInputs[i].status = true;  //给每个输入框添加一个状态，有提示信息则状态为false

            aRegisterFormInputs[i].oninput = function () {
                if (this.value != '') {
                    this.className = 'form-input register-input';
                }
            }
            aRegisterFormInputs[i].onblur = function () {
                //输入框失去焦点时，是否仍要显示提示信息
                for (let j = 0; j < aRegisterFormInputs.length; j++) {
                    if (this.value == '') {
                        this.className = 'form-input register-input error';
                    } else {
                        if (this.status == false) {
                            this.className = 'form-input register-input error';
                        } else {
                            this.className = 'form-input register-input';
                        }
                    }
                }

                //失去焦点，清除提示信息
                oRegUserLabel.className = '';
                oRegPassLabel.className = '';
            }
        }

        //------------------------------------------------------------

        //用户名输入验证
        oRegisterUser.oninput = function () {
            var _thisValue = this.value.replace(/\s/g, "");  //移除所有空格

            //检测是否输入值
            if (_thisValue != '') {
                //检测用户名是否符合规则
                if (testUserName(_thisValue) == 0) {
                    oRegisterUser.style.backgroundColor = '#fff';
                    oRegisterUser.className = 'form-input register-input error';
                    aRegisterFormInputs[0].status = false;
                    bRegiSubmitUser = false;
                } else {
                    //验证用户是否已经被注册
                    ajax('./admin/data.test.php', '?userName=' + _thisValue, fnSuccRegiUser, fnFailRegiUser);
                }
            } else {  //输入框没有值时，表单不能提交，由input的required控制
                oUserExist.style.display = 'none';
                oRegisterUser.style.backgroundColor = '#fff';
                oRegisterUser.className = 'form-input register-input';
                aRegisterFormInputs[0].status = true;
            }
        }

        /**
         * 检测用户输入的用户名是非符合规则
         * @param userName: 用户名
         * @return Number 1：符合规则，0：不符合规则
         */
        function testUserName(userName) {
            //区分大小写，纯字母(4-16位) 或 数字+字母(4-16位) 或 纯数字(9-16位)
            var reUserName = /^(?:[a-zA-Z]{4,16}|[0-9]{9,16}|(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z]{4,16})$/;
            var _thisValue = userName.replace(/\s/g, "");  //移除所有空格

            if (_thisValue.match(reUserName)) {
                return 1;
            } else {
                return 0;
            }
        }

        //邮箱输入验证
        oRegisterEmail.oninput = function () {  //邮箱验证不用去除空格（required属性会自动验证）
            var _thisValue = this.value;

            //验证邮箱是否已经存在
            if (_thisValue != '') {
                if (testEmail(_thisValue) == 1) {
                    //验证邮箱是否已经存在
                    ajax('./admin/data.test.php', '?email=' + _thisValue, fnSuccRegiEmail, fnFailRegiEmail);
                } else {
                    oRegisterEmail.style.backgroundColor = '#fff';
                    oRegisterEmail.className = 'form-input register-input error';
                    bRegiSubmitEmail = false;
                    aRegisterFormInputs[1].status = false;
                }
            } else {
                oEmailExist.style.display = 'none';
                oRegisterEmail.style.backgroundColor = '#fff';
                oRegisterEmail.className = 'form-input register-input';
                aRegisterFormInputs[1].status = true;
            }
        }

        //注册时，ajax检测用户名成功的回调函数
        function fnSuccRegiUser() {
            console.log('被注册');
            oUserExist.style.display = 'block';
            oRegisterUser.style.backgroundColor = '#fff';
            oRegisterUser.className = 'form-input register-input error';
            bRegiSubmitUser = false;
            aRegisterFormInputs[0].status = false;
        }
        //注册时，ajax检测用户名失败的回调函数
        function fnFailRegiUser() {
            console.log('没被注册');
            oUserExist.style.display = 'none';
            oRegisterUser.style.backgroundColor = '#faffbd';
            oRegisterUser.className = 'form-input register-input right';
            bRegiSubmitUser = true;
            aRegisterFormInputs[0].status = true;
        }

        //注册时，ajax检测邮箱成功的回调函数
        function fnSuccRegiEmail() {
            oEmailExist.style.display = 'block';
            oRegisterEmail.style.backgroundColor = '#fff';
            oRegisterEmail.className = 'form-input register-input error';
            bRegiSubmitEmail = false;
            aRegisterFormInputs[1].status = false;
        }
        //注册时，ajax检测邮箱失败的回调函数
        function fnFailRegiEmail() {
            oEmailExist.style.display = 'none';
            oRegisterEmail.style.backgroundColor = '#faffbd';
            oRegisterEmail.className = 'form-input register-input right';
            bRegiSubmitEmail = true;
            aRegisterFormInputs[1].status = true;
        }

        //------------------------------------------------------------

        //密码强度检验条
        var oProgressBar = document.getElementById('oProgressBar');
        var passwdGrade = oProgressBar.getElementsByTagName('div');

        //检测输入密码的强度
        oRegisterPasswd.oninput = function () {
            var _thisValue = this.value.replace(/\s/g, "");  //移除所有空格

            for (let i = 0; i < passwdGrade.length; i++) {
                passwdGrade[i].style.backgroundColor = '#fff';
            }

            if (_thisValue != '') {
                for (let i = 0; i < passwdGrade.length; i++) {
                    if (testPassword(_thisValue) == 1) {
                        passwdGrade[0].style.backgroundColor = 'rgb(5, 194, 5)';
                        this.style.backgroundColor = '#faffbd';
                        this.className = 'form-input register-input';
                        bRegiSubmitPass = true;
                        aRegisterFormInputs[2].status = true;
                    } else if (testPassword(_thisValue) == 2) {
                        passwdGrade[0].style.backgroundColor = 'rgb(5, 194, 5)';
                        passwdGrade[1].style.backgroundColor = 'rgb(226, 226, 41)';
                        this.style.backgroundColor = '#faffbd';
                        this.className = 'form-input register-input';
                        bRegiSubmitPass = true;
                        aRegisterFormInputs[2].status = true;
                    } else if (testPassword(_thisValue) == 3) {
                        passwdGrade[0].style.backgroundColor = 'rgb(5, 194, 5)';
                        passwdGrade[1].style.backgroundColor = 'rgb(226, 226, 41)';
                        passwdGrade[2].style.backgroundColor = 'rgb(221, 40, 40)';
                        this.style.backgroundColor = '#faffbd';
                        this.className = 'form-input register-input';
                        bRegiSubmitPass = true;
                        aRegisterFormInputs[2].status = true;
                    } else if (testPassword(_thisValue) == 0) {
                        this.style.backgroundColor = '#fff';
                        this.className = 'form-input register-input error';
                        bRegiSubmitPass = false;
                        aRegisterFormInputs[2].status = false;
                    }
                }
            } else {
                this.style.backgroundColor = '#fff';
                this.className = 'form-input register-input';
                aRegisterFormInputs[2].status = true;
            }
        }

        oRegisterPasswdAgain.oninput = function () {
            var _thisValue = this.value.replace(/\s/g, "");  //移除所有空格

            if (_thisValue != '') {
                this.style.backgroundColor = '#faffbd';
                this.className = 'form-input register-input';
            } else {
                this.style.backgroundColor = '#fff';
                this.className = 'form-input register-input';
            }
        }

        /**
         * 邮箱规则检测
         * @param email 邮箱字符串
         * @return 1 邮箱符合规则，{0} 邮箱不符合规则
         */
        function testEmail(email) {
            var reEmail = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;

            if (email.match(reEmail)) {
                return 1;
            } else {
                return 0;
            }
        }

        /**
         * 三级密码强度（都是6-16位）：
         * 
         * 一级：纯数字，纯字母，纯特殊字符
         * 二级：上述中的两种组合
         * 三级：纯数字+纯字母+纯特殊字符
         * 
        */
        /**
         * 检测用户输入密码的强度
         * @param passwd：密码
         * @return Number 0：密码不符合规定，1：密码强度为一级，2：密码强度为二级，3：密码强度为三级
         */
        function testPassword(passwd) {
            // 一级密码(6-16位)：纯数字，纯字母，纯特殊字符
            var rePassword1 = /^(?:[0-9]{6,16}|[a-zA-Z]{6,16}|[\$%&#\*;,\.!\?]{6,16})$/;

            //二级密码（6-16位）：数字+字母，数字+特殊字符，字母+特殊字符
            var rePassword2 = /^(?![\$%&#\*;,\.!\?]+$)(?![a-zA-Z]+$)(?![0-9]+$)(?:[a-zA-Z0-9]{6,16}$|[a-zA-Z\$%&#\*;,\.!\?]{6,16}$|[0-9\$%&#\*;,\.!\?]{6,16}$)$/;

            //三级密码（6-16位）：数字+字母+特殊字符 全有
            var rePassword3 = /^(?![\$%&#\*;,\.!\?]+$)(?![a-zA-Z]+$)(?![0-9]+$)(?![a-zA-Z\$%&#\*;,\.!\?]+$)(?![0-9\$%&#\*;,\.!\?]+$)(?![a-zA-Z0-9]+$)[a-zA-Z0-9\$%&#\*;,\.!\?]{6,16}$/;

            if (passwd.match(rePassword1)) {
                return 1;
            } else if (passwd.match(rePassword2)) {
                return 2;
            } else if (passwd.match(rePassword3)) {
                return 3;
            } else {
                return 0;
            }
        }

        //注册表单提交（验证两次输入的密码是否一致）
        oRegisterForm.onsubmit = function () {
            var _RegisterPasswdValue = oRegisterPasswd.value.replace(/\s/g, "");  //移除所有空格
            var _RegisterPasswdAgainValue = oRegisterPasswdAgain.value.replace(/\s/g, "");  //移除所有空格

            //两次输入的密码不一致，阻止表单提交
            if (_RegisterPasswdValue != _RegisterPasswdAgainValue) {
                alertConfirm('两次输入的密码不一致');

                return false;
            }
            
            //用户名，邮箱，密码必须要全符合规定，才可以提交
            if (!(bRegiSubmitUser == true && bRegiSubmitEmail == true && bRegiSubmitPass == true)) {
                alertConfirm('用户名或密码或邮箱错误');

                return false;
            }
        }

    });  //layui end
};  //window.onload end

/**
 * 将用户输入到表单中的数据储存为cookie
 * @param key：cookie的键，value：cookie的值，iDate：cookie的过期时间
 */
function setCookie(key, value, iDate) {
    var oDate = new Date();

    oDate.setDate(oDate.getDate() + iDate);

    document.cookie = key + '=' + value + ';expires=' + oDate;
}

/**
 * 获取储存在本地的cookie
 * @param key：cookie的键
 * @return String
 */
function getCookie(key) {
    var arr = document.cookie.split('; ');

    for (let i = 0; i < arr.length; i++) {
        var arr2 = arr[i].split('=');

        if (arr2[0] == key) {
            return arr2[1];
        }
    }

    return '';
}

/**
 * 删除指定的cookie
 * @param key: 指定cookie的键
 */
function removeCookie(key) {
    setCookie(key, false, -1);
}

/**
 * 对用户的提示框
 * @param name：要提示的文字
*/
function alertConfirm(name) {
    layer.open({
        skin: 'passwdError'  //自定义的layui样式
        , type: 1  //弹出框类型
        , title: false //不显示标题栏
        , closeBtn: false  //是否有关闭按钮
        , area: ['300px', '200px']  //宽和高
        , shade: 0.6  //遮罩透明度
        , shadeClose: true  //点击遮罩是否关闭
        , id: '19980616' //设定一个id，防止重复弹出
        , btn: '我知道了'  //按钮上的文字
        , btnAlign: 'c'  //按钮的对齐方式
        , moveType: 0 //拖拽模式，0或者1
        , content: '<div style="height: 140px; line-height: 190px; text-align: center; font-size: 15px; font-weight: 800; overflow: hidden;">' + name + '</div>'  //弹出框的内容（可以用html，css的语法）
    });
}