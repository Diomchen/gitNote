// wangEditor表情配置
editor.customConfig.emotions = [
    {
        // tab 的标题
        title: '大黄脸',
        // type -> 'emoji' / 'image'
        type: 'image',
        // content -> 数组
        content: [
            {
                // alt: "[坏笑]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/4d/2018new_huaixiao_org.png"
            }, {
                // alt: "[笑cry]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/4a/2018new_xiaoku_org.png"
            }, {
                // alt: "[馋嘴]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/fa/2018new_chanzui_org.png"
            }, {
                // alt: "[拜拜]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/fd/2018new_baibai_org.png"
            }, {
                // alt: "[右哼哼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/c1/2018new_youhengheng_org.png"
            }, {
                // alt: "[左哼哼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/43/2018new_zuohengheng_org.png"
            }, {
                // alt: "[怒骂]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/87/2018new_zhouma_org.png"
            }, {
                // alt: "[顶]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/ae/2018new_ding_org.png"
            }, {
                // alt: "[微笑]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/e3/2018new_weixioa02_org.png"
            }, {
                // alt: "[偷笑]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/71/2018new_touxiao_org.png"
            }, {
                // alt: "[舔屏]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/3e/2018new_tianping_org.png"
            }, {
                // alt: "[亲亲]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/2c/2018new_qinqin_org.png"
            }, {
                // alt: "[太开心]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/1e/2018new_taikaixin_org.png"
            }, {
                // alt: "[挤眼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/43/2018new_jiyan_org.png"
            }, {
                // alt: "[衰]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/a2/2018new_shuai_org.png"
            }, {
                // alt: "[感冒]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/40/2018new_kouzhao_org.png"
            }, {
                // alt: "[可怜]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/96/2018new_kelian_org.png"
            }, {
                // alt: "[汗]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/28/2018new_han_org.png"
            }, {
                // alt: "[色]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/9d/2018new_huaxin_org.png"
            }, {
                // alt: "[可爱]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/09/2018new_keai_org.png"
            }, {
                // alt: "[钱]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/a2/2018new_qian_org.png"
            }, {
                // alt: "[思考]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/30/2018new_sikao_org.png"
            }, {
                // alt: "[生病]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/3b/2018new_shengbing_org.png"
            }, {
                // alt: "[困]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/3c/2018new_kun_org.png"
            }, {
                // alt: "[互粉]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/86/2018new_hufen02_org.png"
            }, {
                // alt: "[睡]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/e2/2018new_shuijiao_org.png"
            }, {
                // alt: "[并不简单]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/aa/2018new_bingbujiandan_org.png"
            }, {
                // alt: "[害羞]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/c1/2018new_haixiu_org.png"
            }, {
                // alt: "[费解]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/2a/2018new_wenhao_org.png"
            }, {
                // alt: "[挖鼻]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/9a/2018new_wabi_org.png"
            }, {
                // alt: "[悲伤]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/ee/2018new_beishang_org.png"
            }, {
                // alt: "[打脸]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/cb/2018new_dalian_org.png"
            }, {
                // alt: "[抓狂]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/17/2018new_zhuakuang_org.png"
            }, {
                // alt: "[哈哈]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/8f/2018new_haha_org.png"
            }, {
                // alt: "[傻眼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/dd/2018new_shayan_org.png"
            }, {
                // alt: "[晕]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/07/2018new_yun_org.png"
            }, {
                // alt: "[鄙视]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/da/2018new_bishi_org.png"
            }, {
                // alt: "[哼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/7c/2018new_heng_org.png"
            }, {
                // alt: "[哈欠]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/55/2018new_dahaqian_org.png"
            }, {
                // alt: "[泪]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6e/2018new_leimu_org.png"
            }, {
                // alt: "[怒]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/f6/2018new_nu_org.png"
            }, {
                // alt: "[闭嘴]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/62/2018new_bizui_org.png"
            }, {
                // alt: "[疑问]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/b8/2018new_ningwen_org.png"
            }, {
                // alt: "[白眼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/ef/2018new_landelini_org.png"
            }, {
                // alt: "[吐]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/08/2018new_tu_org.png"
            }, {
                // alt: "[黑线]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/a3/2018new_heixian_org.png"
            }, {
                // alt: "[委屈]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/a5/2018new_weiqu_org.png"
            }, {
                // alt: "[笑而不语]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/2d/2018new_xiaoerbuyu_org.png"
            }, {
                // alt: "[阴险]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/9e/2018new_yinxian_org.png"
            }, {
                // alt: "[嘘]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/b0/2018new_xu_org.png"
            }, {
                // alt: "[嘻嘻]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/33/2018new_xixi_org.png"
            }, {
                // alt: "[爱你]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/f6/2018new_aini_org.png"
            }, {
                // alt: "[吃惊]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/49/2018new_chijing_org.png"
            }, {
                // alt: "[捂眼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/10/2018new_wu_org.png"
            }, {
                // alt: "[鼓掌]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6e/2018new_guzhang_org.png"
            }, {
                // alt: "[憧憬]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/c9/2018new_chongjing_org.png"
            }, {
                // alt: "[酷]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/c4/2018new_ku_org.png"
            }, {
                // alt: "[失望]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/aa/2018new_shiwang_org.png"
            }, {
                // alt: "[吃瓜]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/01/2018new_chigua_org.png"
            }, {
                // alt: "[捂脸哭]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/83/2018new_kuxiao_org.png"
            }
        ]
    },
    {
        // tab 的标题
        title: '其他表情',
        // type -> 'emoji' / 'image'
        type: 'image',
        // content -> 数组
        content: [
            {
                // alt: "[赞]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/8a/2018new_good_org.png"
            }, {
                // alt: "[弱]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/3d/2018new_ruo_org.png"
            }, {
                // alt: "[耶]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/29/2018new_ye_org.png"
            }, {
                // alt: "[勾引]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/42/2018new_guolai_org.png"
            }, {
                // alt: "[握手]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/e9/2018new_woshou_org.png"
            }, {
                // alt: "[加油]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/9f/2018new_jiayou_org.png"
            }, {
                // alt: "[爱你]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/1d/2018new_hahashoushi_org.png"
            }, {
                // alt: "[拳头]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/86/2018new_quantou_org.png"
            }, {
                // alt: "[ok]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/45/2018new_ok_org.png"
            }, {
                // alt: "[NO]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/1e/2018new_no_org.png"
            }, {
                // alt: "[作揖]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/e7/2018new_zuoyi_org.png"
            }, {
                // alt: "[心]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/8a/2018new_xin_org.png"
            }, {
                // alt: "[伤心]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6c/2018new_xinsui_org.png"
            }, {
                // alt: "[中国赞]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6d/2018new_zhongguozan_org.png"
            }, {
                // alt: "[doge]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/a1/2018new_doge02_org.png"
            }, {
                // alt: "[喵喵]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/7b/2018new_miaomiao_org.png"
            }, {
                // alt: "[二哈]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/22/2018new_erha_org.png"
            }, {
                // alt: "[抱抱]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/42/2018new_baobao_org.png"
            }, {
                // alt: "[米奇比心]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/31/mickey_bixin_org.png"
            }, {
                // alt: "[浮云]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/61/2018new_yunduo_org.png"
            }, {
                // alt: "[男孩儿]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/0a/2018new_nanhai_org.png"
            }, {
                // alt: "[女孩儿]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/39/2018new_nvhai_org.png"
            }, {
                // alt: "[奥特曼]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/c6/2018new_aoteman_org.png"
            }, {
                // alt: "[礼物]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/0e/2018new_liwu_org.png"
            }, {
                // alt: "[飞机]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/4a/2018new_feiji_org.png"
            }, {
                // alt: "[干杯]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/40/2018new_ganbei_org.png"
            }, {
                // alt: "[钟]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/8e/2018new_zhong_org.png"
            }, {
                // alt: "[浪]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/46/2018new_xinlang_org.png"
            }, {
                // alt: "[蜡烛]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/16/2018new_lazhu_org.png"
            }, {
                // alt: "[月亮]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/d5/2018new_yueliang_org.png"
            }, {
                // alt: "[蛋糕]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/f9/2018new_dangao_org.png"
            }, {
                // alt: "[音乐]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/1f/2018new_yinyue_org.png"
            }, {
                // alt: "[猪头]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/1c/2018new_zhutou_org.png"
            }, {
                // alt: "[鲜花]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/d4/2018new_xianhua_org.png"
            }, {
                // alt: "[太阳]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/cd/2018new_taiyang_org.png"
            }, {
                // alt: "[下雨]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/7e/2018new_yu_org.png"
            }, {
                // alt: "[兔子]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/c6/2018new_tuzi_org.png"
            }, {
                // alt: "[骷髅]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/a1/2018new_kulou_org.png"
            }, {
                // alt: "[喜]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/e0/2018new_xizi_org.png"
            }, {
                // alt: "[威武]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/14/2018new_weiwu_org.png"
            }, {
                // alt: "[钢铁侠]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/44/fulian3_gangtiexia01_org.png"
            }, {
                // alt: "[美国队长]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/1e/fulian3_meiguoduizhang01_org.png"
            }, {
                // alt: "[浩克]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/5a/fulian3_haoke01_org.png"
            }, {
                // alt: "[雷神]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/1f/fulian3_leishen01_org.png"
            }, {
                // alt: "[洛基]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/69/fulian3_luoji01_org.png"
            }, {
                // alt: "[蜘蛛侠]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/7e/fulian3_zhizhuxia01_org.png"
            }, {
                // alt: "[奇异博士]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/a3/fulian3_qiyiboshi01_org.png"
            }, {
                // alt: "[黑寡妇]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/78/fulian3_heiguafu01_org.png"
            }, {
                // alt: "[冬兵]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/5e/fulian3_dongbing01_org.png"
            }, {
                // alt: "[格鲁特]",
                alt: "",
                src: "http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/de/fulian3_gelute01_org.png"
            }
        ]
    },
    {
        // tab 的标题
        title: 'emoji',
        // type -> 'emoji' / 'image'
        type: 'emoji',
        // content -> 数组
        content: [
            '😀', '😁', '😂', '😃', '😄', '😅', '😆', '😉',
            '😊', '😋', '😎', '😍', '😘', '😗', '😙', '😚',
            '😇', '😐', '😑', '😶', '😏', '😣', '😥', '😮',
            '😯', '😪', '😫', '😴', '😌', '😛', '😜', '😝',
            '😒', '😓', '😔', '😕', '😲', '😷', '😖', '😞',
            '😟', '😤', '😢', '😭', '😦', '😧', '😨', '😬',
            '😰', '😱', '😳', '😵', '😡', '😠', '🎅', '🙏',
            '💏', '👪', '💪', '👈', '👉', '👆', '👇', '✋',
            '👌', '👍', '👎', '👊', '👀', '💍', '🌚', '🌝',
            '🌎', '⚡', '🔥', '💧', '⚽', '⚾', '🏀',
            '🏈', '🎱', '👿', '💀', '💣']
    }
]