<?php
    session_start();
    require_once('./admin/connect.php');

    //用户注销
    if (!isset($_SESSION['userid'])) {
        echo "<script>alert('您已注销，请重新登录！'); location.href='index.php'</script>";
        exit;
    }

    //用户还未激活
    if (isset($_SESSION['userid'])) {
        $testActive = "select * from user where id=".$_SESSION['userid'];
        $res = mysqli_query($con, $testActive);
        if (mysqli_fetch_assoc($res)['status'] == '0') {
            echo "<script>alert('用户还未激活，请前往邮箱激活！'); location.href='index.php'</script>";
            exit;
        }
    }
    
    $userid = $_SESSION['userid'];

    $sql = "select * from note where userid=$userid order by updateTime desc";
    $result = mysqli_query($con, $sql);

    if ($result && mysqli_num_rows($result)) {
        while($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }
    } else {
        $data = array();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>gitNote</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/iconfont.css">
    <link rel="stylesheet" href="layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="css/bin.css">
    <script type="text/javascript" src="js/bin.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
</head>
<body>
    <!-- layui框架 -->
    <script src="layui/layui.js"></script>
    
    <aside>
        <div id="oNoteBack" class="unSelect">返回</div>
    </aside>
    
    <!-- 笔记页面左边栏 -->
    <section id="bin" class="filter-blur">
        <header class="bin-header">
            <p class="unSelect">回收站</p>
        </header>

        <main class="bin-main">
            <!-- 隐藏域 -->
            <div class="userid" name="<?php echo $userid?>" style="display: none;"></div>
            
            <ul id="oBin-main-ul">
                <?php
                    if(!empty($data)) {
                        rsort($data);  //对数组排序

                        foreach($data as $value) {
                            if ($value['isDelete'] == 1) {
                ?>

                <li name="<?php echo $value['id']?>" class="bin-main-li">
                    <h5><?php echo $value['header']?></h5>
                    <div>
                        <span class="oDeleted unSelect">彻底删除</span>
                        <span class="oRestore unSelect">还原</span>
                    </div>

                    <main><p><?php echo emoji_decode($value['content'])?></p></main>
                    
                    <i><?php echo $value['updateTime']?></i>
                    <hr>
                </li>

                <?php
                            }

                        }

                    }

                    //对emoji表情转反义
                    function emoji_decode($str){
                        $strDecode = preg_replace_callback('|\[\[EMOJI:(.*?)\]\]|', function($matches){  
                            return rawurldecode($matches[1]);
                        }, $str);

                        return $strDecode;
                    }
                ?>
            </ul>
        </main>
    </section>    
</body>
</html>