<?php
    session_start();
    require_once('./admin/connect.php');

    //用户注销
    if (!isset($_SESSION['userid'])) {
        echo "<script>alert('您已注销，请重新登录！'); location.href='index.php'</script>";
        exit;
    }

    //用户还未激活
    if (isset($_SESSION['userid'])) {
        $testActive = "select * from user where id=".$_SESSION['userid'];
        $res = mysqli_query($con, $testActive);
        if (mysqli_fetch_assoc($res)['status'] == '0') {
            echo "<script>alert('用户还未激活，请前往邮箱激活！'); location.href='index.php'</script>";
            exit;
        }
    }
    
    $userid = $_SESSION['userid'];

    //查询用户表
    $userSql = "select * from user where id=$userid";
    $userRes = mysqli_query($con, $userSql);
    $userRow = mysqli_fetch_assoc($userRes);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link type="text/css" rel="stylesheet" href="css/user.css">
    <link type="text/css" rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/iconfont.css">
    <script src="js/user.js"></script>
</head>
<body>
    <aside>
        <div id="oNoteBack" class="unSelect">返回</div>
    </aside>
    
    <div id="detail-card">
        <div class="headImg">
            <form class="upload-img-form" action="./admin/note.notebook.handle.php" method="POST" enctype="multipart/form-data">
                <label class="upload-img-label" for="getHeadImgFile">
                    <?php
                        if ($userRow['headImg']) {
                            echo "<img id=\"previewImg\" src=\"".$userRow['headImg']."\" alt=\"\">";
                        } else {
                            echo "<img id=\"previewImg\" src=\"./imgs/user.png\" alt=\"\">";
                        }
                    ?>
                </label>

                <input id="getHeadImgFile" type="file" name="getFile" />
                <!-- 如果选择文件窗口打开慢的话，就用下面的这行 -->
                <!-- <input id="getHeadImgFile" type="file" name="getFile" accept="image/png, image/jpeg, image/gif, image/jpg" /> -->
                <input id="oHeadImg" type="text" name="imgFileText" style="display: none;">

                <button class="up-pic form-submit-btn" type="submit">保存修改</button>
            </form>
        </div>
        <button id="up-pic" class="up-pic">点击更改头像</button>

        <div class="table">
            <div class="table-row">
                <h6>用户名</h6><!--
             --><p><?php echo $userRow['userName'];?></p>
            </div>
            <div class="table-row">
                <h6>邮箱</h6><!--
             --><p><?php echo $userRow['email'];?></p>
            </div>
            <div class="table-row">
                <h6>注册时间</h6><!--
             --><p><?php echo $userRow['createTime'];?></p>
            </div>
        </div>

        <!-- <button id="changePass-btn">点击更改密码</button> -->
    </div>
</body>
</html>