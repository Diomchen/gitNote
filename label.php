<?php
    session_start();
    require_once('./admin/connect.php');

    //用户注销
    if (!isset($_SESSION['userid'])) {
        echo "<script>alert('您已注销，请重新登录！'); location.href='index.php'</script>";
        exit;
    }

    //用户还未激活
    if (isset($_SESSION['userid'])) {
        $testActive = "select * from user where id=".$_SESSION['userid'];
        $res = mysqli_query($con, $testActive);
        if (mysqli_fetch_assoc($res)['status'] == '0') {
            echo "<script>alert('用户还未激活，请前往邮箱激活！'); location.href='index.php'</script>";
            exit;
        }
    }
    
    $userid = $_SESSION['userid'];

    if (isset($_GET['input'])) {
        $input = $_GET['input'];
    }

    //查询标签
    $selectMark = "select * from mark where userid=$userid";
    $markResult = mysqli_query($con, $selectMark);

    if ($markResult && mysqli_num_rows($markResult)) {
        while($markRow = mysqli_fetch_assoc($markResult)) {
            $markData[] = $markRow;
        }
    } else {
        $markData = array();
    }

    //查询笔记
    $selectNote = "select * from note where userid=$userid";
    $selectNoteResult = mysqli_query($con, $selectNote);

    //将每个笔记本中的笔记存入数组
    if ($selectNoteResult && mysqli_num_rows($selectNoteResult)) {
        while($allNoteRow = mysqli_fetch_assoc($selectNoteResult)) {
            $noteData[] = $allNoteRow;
        }
    } else {
        $noteData = array();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>gitNote</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/iconfont.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/label.css">
    <script type="text/javascript" src="js/label.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
</head>
<body>
    <!-- layui框架 -->
    <script src="layui/layui.js"></script>

    <script>
        //进入这个页面刷新一次(用于更新笔记本中的笔记数)
        var curURL = location.href;

        if (curURL.match('num=0')) {
            var newURL = curURL.replace(/num=0/, 'num=1');
            self.location.href = newURL;
        }
    </script>

    <aside>
        <div id="oMarkBack" class="unSelect">返回</div>
    </aside>

    <section id="mark">
        <header  class="mark-header">
            <div class="mark-header-word">
                <p>标签</p>
                
                <button id="oPlusMark"><i class="iconfont icon-add"></i></button>
            </div>

            <div class="search-div">
                <input id="oSearchInput" class="search-input" type="type" placeholder="搜索标签名称" value="<?php 
                    if (isset($input)) {
                        echo $input;
                    } else {
                        echo '';
                    }
                ?>"/>
                <img class="search-logo" src="imgs/search.png" alt="">
            </div>
        </header>

        <main class="mark-main">
            <!-- 隐藏域 -->
            <div class="userid" name="<?php echo $userid?>" style="display: none;"></div>
            
            <!-- 笔记本 -->
            <ul id="oMarkList" class="mark-list">
                <?php
                    if (!empty($markData)) {
                        sort($markData);
                        
                        foreach($markData as $markValue) {
                            if ($markValue['isDelete'] == 0) {
                ?>
                
                <li class="mark-li" name="<?php echo $markValue['id']?>">
                    <div class="mark-li-top">
                        <div class="mark-list-word">
                            <h5 class="unSelect"><i class="iconfont icon-label"></i><?php echo $markValue['markName']?></h5>
                            <p class="noteNumber unSelect">xx条笔记</p>
                        </div>

                        <div class="mark-list-func">
                            <span class="iconfont icon-fenxiang"></span>
                            <span class="iconfont icon-shoucang"></span>
                            <span class="iconfont icon-delete"></span>
                        </div>
                    </div>

                    <ol class="note-list">
                        <?php
                            if (!empty($noteData)) {
                                rsort($noteData);

                                foreach($noteData as $noteValue) {
                                    if ($noteValue['isDelete'] == 0 && $noteValue['markID'] == $markValue['id']) {                                    
                        ?>

                        <li class="note-li" name="<?php echo $noteValue['id']?>">
                            <h6 class="note-li-header"><?php echo $noteValue['header']?></h6>
                            <main><p><?php echo emoji_decode($noteValue['content'])?></p></main>
                            <i><?php echo $noteValue['createTime']?></i>
                        </li>

                        <?php
                                    }
                                }
                            }
                        ?>
                    </ol>
                </li>

                <?php
                            }
                        }
                    }

                    // 对emoji表情转反义
                    function emoji_decode($str){
                        $strDecode = preg_replace_callback('|\[\[EMOJI:(.*?)\]\]|', function($matches){  
                            return rawurldecode($matches[1]);
                        }, $str);

                        return $strDecode;
                    }
                ?>
            </ul>
        </main>
    </section>
</body>
</html>