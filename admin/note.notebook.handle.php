<?php
    session_start();
    require_once('connect.php');

    $userid = $_SESSION['userid'];
    $createTime = date('Y-m-d H:i:s', time());

    if (isset($_GET['bookName'])) {  //创建笔记本
        $bookName = $_GET['bookName'];
        $insertBookSql = "insert into notebook(userid, bookName, createTime) values($userid, '$bookName', '$createTime')";
        
        if (mysqli_query($con, $insertBookSql)) {
            echo "<script>alert('笔记本创建成功！'); window.location.href='../notebook.php?&num=1';</script>";
        } else {
            echo "<script>alert('笔记本创建失败！'); window.history.back();</script>";
        }
    } else if(isset($_POST['imgFileText'])) {  //保存用户头像
        $content = $_POST['imgFileText'];
        $savePath = "../headImg/";  //保存在根目录下的一个文件夹
        $usePath = "./headImg/";  //使用这个文件夹
        base64_image_content($content, $savePath, $usePath, 0);  //图片存到本地服务器上
        $headImg = base64_image_content($content, $savePath, $usePath, 0);

        $saveImgSql = "update user set headImg='$headImg' where id=$userid";

        if (mysqli_query($con, $saveImgSql)) {
            echo "<script>alert('保存成功！'); window.location.href='../user.php';</script>";
        } else {
            echo "<script>alert('保存失败！'); window.history.back();</script>";
        }
    } else {  //保存笔记
        $header = $_POST['header'];
        $content = $_POST['content'];

        //匹配出所有图片的base64码，并存入数组
        $reBase64 = "/data:image.*?;base64.*?\"/";
        preg_match_all($reBase64, $content, $base64Img);

        if (sizeof($base64Img[0]) > 0) {  //有图片，对笔记进行处理
            //转义文本中的emoji
            $reBase64Img = "/<img src=\"data:image.*?;base64.*?>/";  //匹配每个base64编码的图片
            $strText = preg_split($reBase64Img, $content);  //以图片的base64编码为符号切分文本
            foreach($strText as $value) {
                $dealEmojiContent[] = emoji_encode($value);  //将切分出来的每段字符串分别转义（使emoji入库）
            }

            $savePath = "../noteImg/";  //保存在根目录下的一个文件夹
            $usePath = "./noteImg/";  //使用这个文件夹

            for ($i = 0; $i < sizeof($base64Img[0]); $i++) {
                $base64Img[0][$i] = rtrim($base64Img[0][$i],"\"");  //去掉图片编码中"号字符及其后面多余的字符
                base64_image_content($base64Img[0][$i], $savePath, $usePath, $i);  //图片存到本地服务器上

                //将图片的路径拼接成img标签
                $imgUsePath[] = "<img src=\"".base64_image_content($base64Img[0][$i], $savePath, $usePath, $i)."\" style=\"max-width:100%;\">";
            }

            $mergeArray = cross_merge_array($dealEmojiContent, $imgUsePath);  // 交叉合并两个数组到一个数组中

            foreach($mergeArray as $value2) {
                $dealQuotesContent[] = str_replace('\'', '\\\'', $value2);  //在每段字符串中的单引号前面加上转义符号(否则sql语句无法执行)
            }

            $dealtContent = join("", $dealQuotesContent);  //将数组中的所有元素连接起来（得到最终处理好的笔记文本***）
        } else {  //没有图片
            $dealtContent = str_replace('\'', '\\\'', emoji_encode($content));
        }

        //用户输入标题时，默认无标题
        if (!$header) {
            $header = '无标题';
        }

        //查询这个用户的默认笔记本的id（将笔记存入这个默认笔记本）
        $selectUserDefaultBookID = "select * from notebook where userid=$userid and bookName='default'";
        $selectBookIDResult = mysqli_query($con, $selectUserDefaultBookID);
        $bookID = mysqli_fetch_assoc($selectBookIDResult)['id'];

        if (isset($_GET['change'])) {  //用户修改笔记
            $noteid = $_GET['noteid'];
            $sql = "update note set header='$header',content='$dealtContent' where id=$noteid and userid=$userid";
        } else if (isset($_GET['notebookid']) && !isset($_GET['markid'])) {  //用户新保存笔记，选择了笔记本，没选择标签
            $notebookid = $_GET['notebookid'];
            $sql = "insert into note(userid, notebookID, header, content, createTime) values($userid, $notebookid, '$header', '$dealtContent', '$createTime')";
        } else if (isset($_GET['markid']) && !isset($_GET['notebookid'])) {  //用户新保存笔记，选择了标签，没选择笔记
            $markid = $_GET['markid'];
            $sql = "insert into note(userid, markID, header, content, createTime) values($userid, $markid, '$header', '$dealtContent', '$createTime')";
        } else if (isset($_GET['notebookid']) && isset($_GET['markid'])) {  //选择了笔记和标签
            $notebookid = $_GET['notebookid'];
            $markid = $_GET['markid'];
            $sql = "insert into note(userid, markID, notebookID, header, content, createTime) values($userid, $markid, $notebookid, '$header', '$dealtContent', '$createTime')";
        } else { //没有选择笔记本，也没选择标签，保存在默认笔记本里，标签为空
            $sql = "insert into note(userid, notebookID, header, content, createTime) values($userid, $bookID, '$header', '$dealtContent', '$createTime')";
        }
        
        if (mysqli_query($con, $sql)) {
            echo "<script>alert('笔记保存成功'); window.location.href='../note.php'</script>";
        } else {
            echo "<script>alert('笔记保存失败'); window.history.back();</script>";  //跳转回原来的界面，并还原原内容
        }
    }

    /**
     * 将文本中的emoji转义（否则无法入库）
     * @param $str: 要转义的字符
     * @return $strEncode: 将其中的emoji转义后的字符
    */
    function emoji_encode($str){
        $strEncode = '';

        $length = mb_strlen($str,'utf-8');

        for ($i=0; $i < $length; $i++) {
            $_tmpStr = mb_substr($str, $i, 1, 'utf-8');    

            if (strlen($_tmpStr) >= 4){
                $strEncode .= '[[EMOJI:'.rawurlencode($_tmpStr).']]';
            } else {
                $strEncode .= $_tmpStr;
            }
        }

        return $strEncode;
    }

    /**
     * 交叉合并数组
     * @param $arr1: 数组1, $arr2：数组2
     * @return Array: 合并后的数组
    */
    function cross_merge_array($arr1, $arr2) {
        $arr1 = array_values($arr1);
        $arr2 = array_values($arr2);
        $count = max(count($arr1), count($arr2));
        $arr = array();
        for ($i = 0; $i < $count; $i++) {
            if ($i < count($arr1)) $arr[] = $arr1[$i]; // 判断，避免下标越界
            if ($i < count($arr2)) $arr[] = $arr2[$i]; // 判断，避免下标越界
        }
        return $arr;
    }

   /**
     * [将Base64图片转换为本地图片并保存]
     * @param  [Base64] $base64_image_content [要保存的Base64]
     * @param  [目录] $savePath [要保存的路径]
     * @param  [目录] $usePath [要使用的路径]
     */
    function base64_image_content($base64_image_content, $savePath, $usePath, $num){
        //匹配出图片的格式
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)){
            $type = $result[2];
            $new_file = $savePath;
            if(!file_exists($new_file)){
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($new_file, 0700);
            }

            $new_file = $new_file.time().$num.".{$type}";

            if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))){
                return $usePath.time().$num.".{$type}";
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
?>