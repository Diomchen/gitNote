<?php
    session_start();
    require_once('connect.php');

    $createTime = date('Y-m-d H:i:s', time());

    //注销账户
    if (isset($_GET['logout'])) {
        $_SESSION = array();
        
        exit;
    }

    //用户登录
    if (isset($_POST['loginUserName']) && isset($_POST['loginUserName'])) {
        $username = $_POST['loginUserName'];
        $password = md5(trim(preg_replace('/\s/', "", $_POST['loginPasswd'])));

        $userLoginSql = "select * from user where userName='$username' and password='$password'";
        $loginRes = mysqli_query($con, $userLoginSql);

        if ($loginRes && mysqli_num_rows($loginRes)) {
            while($loginRow = mysqli_fetch_object($loginRes)) {
                $_SESSION['userid'] = $loginRow->id;
                $_SESSION['userName'] = $loginRow->userName;
                $_SESSION['password'] = $loginRow->password;
                $_SESSION['email'] = $loginRow->email;
                $_SESSION['login'] = '0';  //只是用于判断用户第几次登录
            }

            echo "<script>window.location.href='../main.php'</script>";
        } else {
            echo "<script>alert('登录失败！'); window.history.back();</script>";
        }

        exit;
    }

    //收藏和删除
    if (isset($_GET['table']) && isset($_GET['userid']) && isset($_GET['id'])) {
        $table = $_GET['table'];
        $userid = $_GET['userid'];
        $id = $_GET['id'];

        $sql = "select * from $table where userid=$userid and id=$id";
        $res = mysqli_query($con, $sql);
    
        //收藏
        if (isSet($_GET['star'])) {
            if (mysqli_fetch_assoc($res)['isStar'] == 0) {  //未收藏
                $isStarUpdateSql = "update $table set isStar=1 where userid=$userid and id=$id";
                mysqli_query($con, $isStarUpdateSql);
                echo "1";
            } else {  //已收藏
                $isStarUpdateSql = "update $table set isStar=0 where userid=$userid and id=$id";
                mysqli_query($con, $isStarUpdateSql);
                echo "0";
            }

            exit;
        }

        //删除
        if (isSet($_GET['delete'])) {
            //彻底删除
            if (isSet($_GET['deleted'])) {

                $deleteSql = "delete from $table where userid=$userid and id=$id";
                mysqli_query($con, $deleteSql);
                exit;
            }

            //删除与还原
            if (mysqli_fetch_assoc($res)['isDelete'] == 0) {
                echo "0";  //没有被删除

                //修改isDelete为1，表示被删除
                $updateSql = "update $table set isDelete=1 where userid=$userid and id=$id";
                mysqli_query($con, $updateSql);
            } else {
                echo "1";  //被删除了

                //修改isDelete为0，表示没有被删除
                $updateSql2 = "update $table set isDelete=0 where userid=$userid and id=$id";
                mysqli_query($con, $updateSql2);
            }

            exit;
        }

        exit;
    }

    //笔记移动到其他笔记本
    if (isset($_GET['col']) && isset($_GET['newID']) && isset($_GET['userid']) && isset($_GET['id'])) {
        $col = $_GET['col'];
        $newID = $_GET['newID'];
        $userid = $_GET['userid'];
        $id = $_GET['id'];
    
        $updateBookSql = "update note set $col=$newID where userid=$userid and id=$id";

        if (mysqli_query($con, $updateBookSql)) {
            echo "1";
        } else {
            echo "0";
        }

        exit;
    }

    if (isSet($_GET['markName'])) {  //创建标签
        $markName = $_GET['markName'];
        $userid = $_SESSION['userid'];
        $insertMarkSql = "insert into mark(userid, markName, createTime) values($userid, '$markName', '$createTime')";
        
        if (mysqli_query($con, $insertMarkSql)) {
            echo "<script>alert('标签创建成功！'); window.location.href='../label.php?&num=1';</script>";
        } else {
            echo "<script>alert('标签创建失败！'); window.history.back();</script>";
        }
    }
?>