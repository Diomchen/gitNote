<?php
    require_once('config.php');

    //连库
    $con = mysqli_connect(HOST, USERNAME, PASSWORD);

    //选库
    mysqli_select_db($con, 'gitnote');

    //设置字符编码
    mysqli_query($con, 'set names UTF8');

    //设置时区为中国
    date_default_timezone_set("PRC");
?>