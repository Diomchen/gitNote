## gitNote

**记录生活，留住感动**

[![MIT License](https://img.shields.io/npm/l/mithril.svg)](https://github.com/liuyib/gitNote/blob/master/LICENSE)

#

### 简介

[gitNote](https://github.com/liuyib/gitNote)是基于javascript、jquery、php开发的Web笔记应用，你可以使用它记录生活中的点点滴滴，易用、开源免费。

### 功能展示

**登录注册页面**

![](https://i.loli.net/2018/08/13/5b713c837ee9e.gif)

**笔记页面功能展示**：

![](https://i.loli.net/2018/08/13/5b713c836c58c.gif)

 - 好用的编辑器
 - 收藏，删除，更改笔记本、标签
 - 笔记太多？使用“模糊搜索”快速查询你的笔记

**其他的功能等你来使用**

![](https://i.loli.net/2018/08/13/5b713c82e09de.png)

#

### 如何使用

#### 运行环境准备

 - apache
 - php
 - MySql，版本至少5.6及以上
 - 主流浏览器（不支持IE）

#### 安装

 直接下载：[https://github.com/liuyib/gitNote/releases](https://github.com/liuyib/gitNote/releases)，解压至你的本地服务器默认网站目录下（比如：Apache的www目录）,重新命名文件夹为：gitNote！！！

#### 使用

 - MySql里执行 gitNoe.sql 文件（会自动创建一个名叫gitnote的数据库，以及所需的表）
 - 修改 gitNote/admin/config.php 文件里的：

    define('PASSWORD', '7baf005e3403764f');  //修改 `7baf005e3403764f` 为你的数据库密码 
 
 - 浏览器里通过localhost访问gitNote的文件夹即可

#

### 关于我们

 - 关注我们的博客：
 
    [https://blog.csdn.net/qq_41139830](https://blog.csdn.net/qq_41139830)，
 
    [https://my.csdn.net/qq_41628088](https://my.csdn.net/qq_41628088)
    
 - 联系我们

    DiomChen: QQ: 153583852

    liuyib: QQ: 1656081615

