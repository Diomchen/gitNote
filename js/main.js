;
$(document).ready(function () {
    var oUserHeadImg = document.getElementsByClassName('user')[0];
    var oUserOption = document.getElementsByClassName('user-option')[0];
    var oUser = document.getElementById('user-option-mine');
    var bUser = false;

    oUserHeadImg.onclick = function (ev) {
        var oEvent = ev || event;
        
        if (bUser == true) {
            oUserOption.style.display = 'none';
            bUser = false;
        } else {
            oUserOption.style.display = 'block';
            bUser = true;
        }

        oEvent.cancelBubble = true;  //取消事件冒泡
    };

    document.onclick = function () {
        oUserOption.style.display = 'none';
        bUser = false;
    };

    //个人中心
    oUser.onclick = function () {
        window.location.href = 'user.php';
    };
    
    var oTimer = document.getElementById('other-func-time');
    var aTimerComponents = oTimer.getElementsByTagName('div');
    var aTimerHMs = aTimerComponents[0].getElementsByTagName('span');
    var aTimerYMDs = aTimerComponents[1].getElementsByTagName('span');

    //时间
    setInterval(function () {
        var oDate = new Date();

        for (let i = 0; i < aTimerHMs.length; i++) {
            aTimerHMs[0].innerHTML = oDate.getHours();
            aTimerHMs[2].innerHTML = oDate.getMinutes();
        }
        for (let i = 0; i < aTimerYMDs.length; i++) {
            aTimerYMDs[0].innerHTML = oDate.getFullYear();
            aTimerYMDs[2].innerHTML = oDate.getMonth() + 1;
            aTimerYMDs[4].innerHTML = oDate.getDate();
        }
    }, 30);

    var oColor = document.getElementById('color');
    var aBlock = oColor.getElementsByTagName('div');
    var co = ['#8779d2', '#79a5d2', '#79d2bd', '#d2a279'];
    var bMove = true;  //点击时，动画是否允许执行，下面都用"加锁"和"解锁"来表示"允许"和"不允许"（防止多次点击动画崩溃）

    //前四个颜色块数组（垃圾桶单独做）
    for (let i = 0; i < aBlock.length; i++) {
        aBlock[i].style.backgroundColor = co[i];
    }

    var oMain = document.getElementById('main-box');
    var aImg = oMain.getElementsByTagName('img');
    var index = -1;

    for (let i = 0; i < aImg.length; i++) {
        aImg[i].onclick = function () {

            // console.log(bMove);  //true  原始处于解锁状态
            if (bMove == true) {  //没有被加锁 

                bMove = false;  //动画开始执行，加锁
                // console.log(bMove);  //false  加锁啦

                //导航栏动画
                $('#other-func').animate({ opacity: "1" }, 1100, function () {
                    $(this).css({ display: "none", top: "-60px" });
                }).css({ zIndex: "-10" });
                
                //这个index主要为后面的图片出现做铺垫
                index = i;

                //前四个颜色块动画
                $('.block').eq(i).animate({ top: "100vh", opacity: "0.3" }, 260)//卡时间，并调进场透明度
                    .animate({ top: "0vh", opacity: "1" }, 540)
                    .animate({ height: "100vh" }, 300)//卡时间
                    .animate({ height: "0vh", opacity: "0.8" }, 440);

                //去线条
                $('.w-box').css("border", "0");

                $('.ext-num').animate({ fontSize: "20px" }, 500)
                    .animate({ top: "60vh", opacity: "0", zIndex: "4" }, 700);

                $('.ext-sign').animate({ fontSize: "25px" }, 550)
                    .animate({ opacity: "0", zIndex: "4" }, 700);
                //条块向上移动动画（四个，不含垃圾桶）
                for (let j = 0; j < aImg.length; j++) {
                    var t;
                    if (j == 0) {
                        t = Math.floor(Math.random() * (3 + 1));

                        //白条自身移动（下同）
                        $('.w-box').eq(t).animate({ borderLeft: "1px solid #f2f2f2" }, 700)//卡时间
                            .animate({ height: "0" }, 300);
                        $('.strip-img').eq(t).animate({ width: "100%" }, 900)//卡时间
                            .animate({ marginTop: "-200%", opacity: "0" }, 200);
                        //白条数字与字母消失动画（下同）
                        $('.strip-num').eq(t).animate({ fontSize: "20px" }, 500)//卡时间
                            .animate({ top: "40vh" }, 500)
                            .animate({ top: "0" }, 200)
                            .animate({ opacity: "0" }, 100);
                        $('.strip-sign').eq(t).animate({ fontSize: "25px" }, 550)//卡时间
                            .animate({ top: "40vh" }, 500)
                            .animate({ top: "0", opacity: "0" }, 120)
                    }
                    if (t != 0 || t != j) {
                        $('.w-box').eq(j).animate({ borderLeft: "1px solid #f2f2f2" }, 800 + (j + 3) % 3 * 30)//卡时间
                            .animate({ height: "0" }, 400 + (j + 3) % 3 * 30);
                        $('.strip-img').eq(j).animate({ width: "100%" }, 1000 + (j + 3) % 3 * 60)//卡时间
                            .animate({ marginTop: "-200%", opacity: "0" }, 200);

                        $('.strip-num').eq(j).animate({ fontSize: "20px" }, 550 + (j + 3) % 3 * 30)//卡时间
                            .animate({ top: "40vh" }, 500)
                            .animate({ top: "0" }, 200)
                            .animate({ opacity: "0" }, 100);
                        $('.strip-sign').eq(j).animate({ fontSize: "25px" }, 600 + (j + 3) % 3 * 30)//卡时间
                            .animate({ top: "40vh" }, 550 + (j + 3) % 3 * 30)
                            .animate({ top: "0", opacity: "0" }, 200 + (j + 3) % 3 * 30)
                    }
                }

                //背景大图
                $('.all-pic').eq(index).css({ display: "block", zIndex: "0" });
                $('.all-pic').eq(index).animate({ height: "100vh" }, 1200)
                    .animate({ opacity: "1" }, 300);

                //最后一个垃圾桶向上移动动画（受白条控制）
                $('.r-box').animate({ borderLeft: "1px solid #f2f2f2" }, 700)//卡时间
                    .animate({ height: "0" }, 300);
                $('.Bin').animate({ width: "100%" }, 100)//卡时间
                    .animate({ height: "90%" }, 300)
                    .animate({ height: "10%", marginTop: "0" }, 1000);
                $('.back').animate({ zIndex: "3" }, 1200, function () {
                    $(this).css({ color: "#000" });
                }).animate({ top: "0", opacity: "1" }, 400, function () {
                    bMove = true;  //最后一个动画执行完，解锁
                    // console.log(bMove);  //true  解锁啦
                });

                // console.log(bMove);  //false  仍处于加锁状态

            }  //if (bMove == true) end

        }  //aImg[i].onclick = function end

    }

    var oBin = document.getElementsByClassName('Bin')[0];
    var oBinWord = document.getElementsByClassName('ext-sign')[0];

    //垃圾桶动画分开做（受垃圾桶自己控制）
    oBin.onclick = oBinWord.onclick = function () {

        // console.log(bMove); //true  原始处于解锁状态
        if (bMove == true) {  //没有加锁

            bMove = false;  //动画开始执行，加锁
            // console.log(bMove);  //false  加锁啦

            //导航栏动画
            $('#other-func').animate({ opacity: "1" }, 1100, function () {
                $(this).css({ display: "none", top: "-60px" });
            }).css({ zIndex: "-10" });

            //粉色颜色块动画
            $('.re-block').animate({ top: "100vh", opacity: "0.3" }, 260)
                .animate({ top: "0vh", opacity: "1" }, 540)
                .animate({ height: "100vh" }, 300)
                .animate({ height: "0vh", opacity: "0.8" }, 340).css({ zIndex: "-1" });

            //去线条
            $('.w-box').css("border", "0");

            //垃圾桶自身字母与数字动画
            $('.ext-num').animate({ fontSize: "20px" }, 500)
                .animate({ top: "60vh", opacity: "0", zIndex: "4" }, 700);
            $('.ext-sign').animate({ fontSize: "25px" }, 550)
                .animate({ opacity: "0", zIndex: "4" }, 700);
            //其他白条受垃圾桶控制动画
            for (let j = 0; j < aImg.length; j++) {
                var t;
                if (j == 0) {
                    t = Math.floor(Math.random() * (3 + 1));

                    $('.w-box').eq(t).animate({ borderLeft: "1px solid #f2f2f2" }, 700)//卡时间
                        .animate({ height: "0" }, 300);
                    $('.strip-img').eq(t).animate({ width: "100%" }, 900)//卡时间
                        .animate({ marginTop: "-200%", opacity: "0" }, 200);
                    $('.strip-num').eq(t).animate({ fontSize: "20px" }, 500)//卡时间
                        .animate({ top: "40vh" }, 500)
                        .animate({ top: "0" }, 200)
                        .animate({ opacity: "0" }, 100);
                    $('.strip-sign').eq(t).animate({ fontSize: "25px" }, 550)//卡时间
                        .animate({ top: "40vh" }, 500)
                        .animate({ top: "0", opacity: "0" }, 120);
                }
                if (t != 0 || t != j) {
                    $('.w-box').eq(j).animate({ borderLeft: "1px solid #f2f2f2" }, 800 + (j + 3) % 3 * 30)//卡时间
                        .animate({ height: "0" }, 400 + (j + 3) % 3 * 30);
                    $('.strip-img').eq(j).animate({ width: "100%" }, 1000 + (j + 3) % 3 * 60)//卡时间
                        .animate({ marginTop: "-200%", opacity: "0" }, 200);
                    $('.strip-num').eq(j).animate({ fontSize: "20px" }, 550 + (j + 3) % 3 * 30)//卡时间
                        .animate({ top: "40vh" }, 500)
                        .animate({ top: "0" }, 200)
                        .animate({ opacity: "0" }, 100);
                    $('.strip-sign').eq(j).animate({ fontSize: "25px" }, 600 + (j + 3) % 3 * 30)//卡时间
                        .animate({ top: "40vh" }, 550 + (j + 3) % 3 * 30)
                        .animate({ top: "0", opacity: "0" }, 200 + (j + 3) % 3 * 30);
                }
            }

            //垃圾桶自身动画
            $('.r-box').animate({ borderLeft: "1px solid #f2f2f2" }, 700)//卡时间
                .animate({ height: "0" }, 300);
            $('.Bin').animate({ width: "100%" }, 100)//卡时间
                .animate({ height: "90%" }, 300)
                .animate({ height: "10%", marginTop: "0" }, 1000);
            
            $('.back').animate({ opacity: "1" }, 1200, function () {
                $(this).css({ color: "#000" });
            }).animate({ top: "0", opacity: "1" }, 400, function () {
                bMove = true;  //最后一个动画执行完：解锁
                // console.log(bMove);  //true  解锁啦
            });

            // console.log(bMove);  //false  动画没执行完，仍处于加锁状态

        };
    };

});