;
window.onload = function () {
    layui.use(['layer'], function () {
        var layer = layui.layer;
        var oNoteBack = document.getElementById('oNoteBack');
        var oUserid = document.getElementsByClassName('userid')[0].getAttribute('name');
        var aMarkLis = document.getElementsByClassName('mark-list-li');

        //返回按钮
        oNoteBack.onclick = function () {
            window.location.href = 'main.php';
        };

        for (let i = 0; i < aMarkLis.length; i++) {
            var aMarkDeletes = aMarkLis[i].getElementsByClassName('icon-jianhao')[0];
            
            //点击取消收藏按钮
            aMarkDeletes.onclick = function (ev) {
                var oEvent = ev || event;
                var oThisid = this.parentNode.getAttribute('name');
                var oThisTable = this.parentNode.getElementsByTagName('i')[0].getAttribute('name');
                var urlParamHandleStar = '?userid=' + oUserid + '&id=' + oThisid + '&table=' + oThisTable + '&star=true';
                
                alertPrompt(this.parentNode, urlParamHandleStar);

                oEvent.cancelBubble = true;  //阻止事件冒泡（使得点击删除按钮不触发li上的点击事件）
            };

            //点击快捷方式跳转
            aMarkLis[i].onclick = function () {
                var oThisTable = this.getElementsByTagName('i')[0].getAttribute('name');
                var oThisHeader = this.getElementsByTagName('h6')[0].innerText;
                var _thisURL = oThisTable + '.php?input=' + oThisHeader;

                window.location.href = _thisURL;
            };
        }        
    });
};

/**
 * 对用户的提示框
*/
function alertPrompt(obj, urlParam) {
    //示范一个公告层
    layer.open({
        type: 1
        , title: false //不显示标题栏
        , closeBtn: false
        , area: ['400px', '250px']
        , shade: 0.6
        , shadeClose: true  //点击遮罩是否关闭
        , id: '19980616' //设定一个id，防止重复弹出
        , btn: ['确定', '取消']
        , btnAlign: 'c'
        , moveType: 1 //拖拽模式，0或者1
        , content: '<div style="height: 180px; line-height: 190px;font-size: 30px; font-weight: 600; text-align: center;">'
            + '确定要取消收藏吗？'
            + '</div>'
        , success: function () {
            var oGetBtn = document.getElementsByClassName('layui-layer-btn0')[0];
            oGetBtn.onclick = function () {
                // 点击确认之后的操作
                ajax('./admin/data.handle.php', urlParam, fnSuccUnstar(obj));
            };
        }
    });

    var oMarkList = document.getElementById('oMarkList');

    function fnSuccUnstar(obj) {
        oMarkList.removeChild(obj);
    }
}