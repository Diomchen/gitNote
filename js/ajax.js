/**
 * 封装ajax
 * @param url: 连接地址，value: 要发送的字符，fnSUcc：成功的回调函数，fnFail：失败的回调函数
*/
function ajax(url, value, fnSucc, fnFail) {
    //1.创建Ajax对象
    if (window.XMLHttpRequest) {
        var oAjax = new XMLHttpRequest();
    } else {
        var oAjax = new ActiveXObject("Microsoft.XMLHTTP");
    }

    //2.连接服务器（打开和服务器的连接）
    if (value) {
        oAjax.open('GET', url + value, true);
        // console.log(url + value);
    } else {
        oAjax.open('GET', url, true);
    }

    //3.发送
    oAjax.send();

    //4.接收
    oAjax.onreadystatechange = function () {
        if (oAjax.readyState == 4 && oAjax.status == 200) {
            if (oAjax.responseText === '1') {
                if (fnSucc) { fnSucc(); }
            } else {
                if (fnFail) { fnFail(); }
            }
        }
    };
}