;
//下面代码的顺序不要乱改！！！！！！！！！！！
var E = window.wangEditor;
var editor = new E('#note-main-footer-toolbar', '#note-main-footer-editor');
var $noteText = null;
var oNoteHeader = null;

window.onload = function () {
    layui.use(['layer'], function () {
        var layer = layui.layer;

        $noteText = $('#note-text');

        //------------------编辑栏api的一些配置-------------------------

        // 监控变化，将笔记同步更新到 textarea 
        editor.customConfig.onchange = function (html) {
            $noteText.val(html);  //以html形式储存笔记
        };

        // 允许插入图片
        editor.customConfig.uploadImgShowBase64 = true;  //以Base64储存图片

        // 不允许插入网络图片
        editor.customConfig.showLinkImg = false;

        // 创建编辑工具栏和文本输入栏
        editor.create();

        // 初始化笔记输入框的值
        editor.txt.html('<p>开始输入你的笔记吧！</p>');

        // 初始化 textarea 的值
        $noteText.val(editor.txt.html());  //以html形式储存笔记

        var oNoteSideMainUl = document.getElementById('oNote-side-main-ul');
        var aNoteSideMainUlLis = oNoteSideMainUl.getElementsByClassName('note-side-main-li');
        var aNoteSideMainUlH5s = oNoteSideMainUl.getElementsByTagName('h5');
        var aNoteSideMainUlMains = oNoteSideMainUl.getElementsByTagName('main');
        var aNoteSideMainUlIs = oNoteSideMainUl.getElementsByTagName('i');
        oNoteHeader = document.getElementById('oNoteHeader');
        var aNoteSideMainFuncs = document.getElementsByClassName('note-side-main-func');
        var oSearchInput = document.getElementById('oSearchInput');

        showThisNote(oSearchInput.value);

        oSearchInput.oninput = function () {
            //用户更改输入框中的值时，去除url里的参数
            window.history.pushState(null, null, 'note.php');  //页面无刷新更改url

            showThisNote(this.value);
        };

        function showThisNote(value) {
            for (let i = 0; i < aNoteSideMainUlLis.length; i++) {
                var _thisHeader = aNoteSideMainUlH5s[i].innerText;
                var _thisContent = aNoteSideMainUlMains[i].innerText;

                if (_thisHeader.match(value) || _thisContent.match(value)) {
                    aNoteSideMainUlLis[i].style.display = 'block';
                } else {
                    aNoteSideMainUlLis[i].style.display = 'none';
                }
            }
        }

        ////载入页面置空笔记编辑栏的标题输入框
        oNoteHeader.value = '';

        //是否修改笔记
        var bChangeNote = false;

        //点击略缩图查看笔记
        for (let i = 0; i < aNoteSideMainUlLis.length; i++) {
            aNoteSideMainUlH5s[i].onclick = aNoteSideMainUlMains[i].onclick = aNoteSideMainUlIs[i].onclick = function () {
                //未选中的笔记边框变为默认
                for (let j = 0; j < aNoteSideMainUlLis.length; j++) {
                    aNoteSideMainUlLis[j].style.border = '3px solid rgba(255, 255, 255, 0)';
                }
                //选中的笔记
                aNoteSideMainUlLis[i].style.border = '3px solid #c9c9c9';

                //点击略缩图，切换笔记
                editor.txt.html(aNoteSideMainUlMains[i].innerHTML);
                oNoteHeader.value = aNoteSideMainUlH5s[i].innerText;

                bChangeNote = true;
            }

            //收藏功能
            for (let j = 0; j < 3; j++) {
                var oNoteSideMainUlDiv = aNoteSideMainUlLis[i].getElementsByTagName('div')[0];
                var aNoteSideMainUlDivSpans = oNoteSideMainUlDiv.getElementsByTagName('span');

                var oUserid = document.getElementsByClassName('userid')[0].getAttribute('name');  //用户id
                var oNoteid = document.getElementsByClassName('note-side-main-li')[i].getAttribute('name');  //笔记id

                var urlParamSelectStar = '?userid=' + oUserid + '&id=' + oNoteid + '&table=note&star=true';

                ajax('./admin/data.test.php', urlParamSelectStar, fnSuccStarSelect, fnFailStarSelect);

                aNoteSideMainUlDivSpans[j].onclick = function () {
                    var _this = this;

                    if (j == 0) {  //提醒

                        //TODO*************************

                    } else if (j == 1) {  //收藏
                        var oNoteid = this.parentNode.parentNode.getAttribute('name');  //笔记id
                        var urlParamHandleStar = '?userid=' + oUserid + '&id=' + oNoteid + '&table=note&star=true';

                        ajax('./admin/data.handle.php', urlParamHandleStar, fnSuccStar, fnFailStar);
                    } else {  //删除
                        var oNoteid = this.parentNode.parentNode.getAttribute('name');  //笔记id
                        var urlParamHandleDelete = '?userid=' + oUserid + '&id=' + oNoteid + '&table=note&delete=true';

                        // 确认提示框
                        alertPrompt(this.parentNode.parentNode, urlParamHandleDelete);
                    }

                    function fnSuccStar() {
                        _this.className = 'iconfont icon-shoucang iconColorRed';
                    }
                    function fnFailStar() {
                        _this.className = 'iconfont icon-shoucang iconColorGray';
                    }
                }  //aNoteSideMainUlDivSpans[j].onclick = function () {  end

                function fnSuccStarSelect() {
                    aNoteSideMainFuncs[i].getElementsByClassName('icon-shoucang')[0].className = 'iconfont icon-shoucang iconColorRed';
                }
                function fnFailStarSelect() {
                    aNoteSideMainFuncs[i].getElementsByClassName('icon-shoucang')[0].className = 'iconfont icon-shoucang iconColorGray';
                }

            }  //for (let j = 0; j < aNoteSideMainUlDivSpans.length; j++) {  end

        }  //for (let i = 0; i < aNoteSideMainUlLis.length; i++) {  end

        var oPlusNote = document.getElementById('oPlusNote');
        var oChangeNotebook = document.getElementById('oChangeNotebook');
        var aChangeNotebookLis = oChangeNotebook.getElementsByClassName('selectNotebook-ul-li');  //笔记本列表
        var aChangeLabelLis = oChangeNotebook.getElementsByClassName('selectLabel-ul-li');  //标签列表
        var oShowBookName = document.getElementById('oShowBookName');  //点击显示笔记本列表
        var oShowlabelName = document.getElementById('oShowLabelName');  //点击显示标签列表
        var oAllBookName = document.getElementById('oAllBookName');  //笔记本列表
        var oAllLabelName = document.getElementById('oAllLabelName');  //标签列表
        var _curNotebookid = '-1';  //当前点击的笔记所在的笔记本id
        var _curNotebook = null;  //当前笔记所在的笔记本
        var newNotebookid = '-1';  //点击新的笔记本的id
        var bBookNameShow = false;  //是否显示笔记本列表

        var _curMarkid = '-1';  //当前点击的笔记所在的标签id
        var _curMark = null;  //当前笔记所在的标签
        var newMarkid = '-1';  //点击新的标签的id
        var bMarkNameShow = false;  //是否显示标签列表

        var oThisNoteid = '';  //选中的笔记

        //新建笔记
        oPlusNote.onclick = function test() {
            oNoteHeader.value = '';
            editor.txt.html('<p>开始输入你的笔记吧！</p>');

            bChangeNote = false;  //没有更改笔记
            oThisNoteid = '-1';  //没有选中笔记
            _curNotebookid = '-1';  //没有选中笔记本
            _curMarkid = '-1';  //没有选中标签

            //重置笔记本列表的样式
            for (let j = 0; j < aChangeNotebookLis.length; j++) {
                var _thisNotebooks = aChangeNotebookLis[j].getElementsByTagName('span');

                _thisNotebooks[0].className = 'selectNotebook-ul-bookName unSelect';
                _thisNotebooks[1].className = 'selectNotebook-ul-logo';
            }

            //重置标签列表的样式
            for (let j = 0; j < aChangeLabelLis.length; j++) {
                var _thisMarks = aChangeLabelLis[j].getElementsByTagName('span');

                _thisMarks[0].className = 'selectLabel-ul-labelName unSelect';
                _thisMarks[1].className = 'selectLabel-ul-logo';
            }
        }

        //对笔记所在的li遍历
        for (let i = 0; i < aNoteSideMainUlLis.length; i++) {
            aNoteSideMainUlLis[i].onclick = function () {
                _curNotebookid = this.getElementsByClassName('notebookID')[0].getAttribute('name');  //获取这个笔记所在的笔记本id
                _curNotebook = this.getElementsByClassName('notebookID')[0];  //获取这个笔记所在的笔记本对象

                _curMarkid = this.getElementsByClassName('markID')[0].getAttribute('name');  //获取这个笔记所在的标签id
                _curMark = this.getElementsByClassName('markID')[0];  //获取这个笔记所在的标签对象

                oThisNoteid = this.getAttribute('name');  //要移动的笔记id
            };
        }

        //移动笔记所在的笔记本
        for (let i = 0; i < aChangeNotebookLis.length; i++) {
            aChangeNotebookLis[i].onclick = function () {
                newNotebookid = this.getAttribute('name');  //移动到的笔记本的id

                //三个参数：用户id，笔记id，移动到的笔记本的id
                var noteMoveParam = '?userid=' + oUserid + '&id=' + oThisNoteid + '&col=notebookID&newID=' + newNotebookid;

                //更改笔记时选择笔记本
                if (_curNotebookid != '-1') {
                    if (newNotebookid != _curNotebookid) {
                        ajax('./admin/data.handle.php', noteMoveParam, fnSuccMove('移动'));

                        _curNotebook.setAttribute('name', newNotebookid);//改变这个笔记的name属性
                        _curNotebookid = newNotebookid; //更新这个笔记的笔记本id
                    }
                } else { // 新建笔记选择笔记本
                    for (let j = 0; j < aChangeNotebookLis.length; j++) {
                        var _thisNotebooks = aChangeNotebookLis[j].getElementsByTagName('span');

                        if (aChangeNotebookLis[j].getAttribute('name') == newNotebookid) {
                            _thisNotebooks[0].className = 'selectNotebook-ul-bookName curStyle unSelect';
                            _thisNotebooks[1].className = 'selectNotebook-ul-logo curStyle iconfont icon-right';
                        } else {
                            _thisNotebooks[0].className = 'selectNotebook-ul-bookName unSelect';
                            _thisNotebooks[1].className = 'selectNotebook-ul-logo';
                        }
                    }
                }
            };
        }
        
        //移动笔记所在的标签
        for (let i = 0; i < aChangeLabelLis.length; i++) {
            aChangeLabelLis[i].onclick = function () {
                newMarkid = this.getAttribute('name');  //移动到的标签的id

                //三个参数：用户id，笔记id，移动到的标签的id
                var noteMoveParam = '?userid=' + oUserid + '&id=' + oThisNoteid + '&col=markID&newID=' + newMarkid;

                //更改笔记时选择标签
                if (_curMarkid != '-1') {
                    if (newMarkid != _curMarkid) {
                        ajax('./admin/data.handle.php', noteMoveParam, fnSuccMove('添加标签'));

                        _curMark.setAttribute('name', newMarkid);//改变这个笔记的name属性
                        _curMarkid = newMarkid; //更新这个笔记的标签id
                    }
                } else { // 新建笔记选择标签
                    for (let j = 0; j < aChangeLabelLis.length; j++) {
                        var _thisMarks = aChangeLabelLis[j].getElementsByTagName('span');

                        if (aChangeLabelLis[j].getAttribute('name') == newMarkid) {
                            _thisMarks[0].className = 'selectLabel-ul-labelName curStyle unSelect';
                            _thisMarks[1].className = 'selectLabel-ul-logo curStyle iconfont icon-right';
                        } else {
                            _thisMarks[0].className = 'selectLabel-ul-labelName unSelect';
                            _thisMarks[1].className = 'selectLabel-ul-logo';
                        }
                    }
                }
            };
        }

        //笔记移动笔记本成功
        function fnSuccMove(str) {
            layer.msg('笔记'+ str +'成功', {
                time: 2000 //2s后自动关闭
                , offset: [100, $(window).width()/2]
            });
        }

        //点击显示笔记本列表按钮
        oShowBookName.onclick = function (ev) {
            oAllLabelName.style.display = 'none';
            bMarkNameShow = false;
            var oEvent = ev || event;

            if (bBookNameShow == false) {
                oAllBookName.style.display = 'block';
                bBookNameShow = true;
            }
            
            for (let j = 0; j < aChangeNotebookLis.length; j++) {
                var _thisNotebooks = aChangeNotebookLis[j].getElementsByTagName('span');
                if (_curNotebookid != '-1') {
                    //显示当前选中的笔记所在的笔记本
                    if (aChangeNotebookLis[j].getAttribute('name') == _curNotebookid) {
                        _thisNotebooks[0].className = 'selectNotebook-ul-bookName curStyle unSelect';
                        _thisNotebooks[1].className = 'selectNotebook-ul-logo curStyle iconfont icon-right';
                    } else {
                        _thisNotebooks[0].className = 'selectNotebook-ul-bookName unSelect';
                        _thisNotebooks[1].className = 'selectNotebook-ul-logo';
                    }
                }
            }
            
            oEvent.cancelBubble = true;  //取消事件冒泡
        };
        //点击显示标签列表按钮
        oShowlabelName.onclick = function (ev) {
            oAllBookName.style.display = 'none';
            bBookNameShow = false;
            var oEvent = ev || event;

            if (bMarkNameShow == false) {
                oAllLabelName.style.display = 'block';
                bMarkNameShow = true;
            }
            
            for (let j = 0; j < aChangeLabelLis.length; j++) {
                var _thisMarks = aChangeLabelLis[j].getElementsByTagName('span');
                if (_curMarkid != '-1') {
                    //显示当前选中的笔记所在的标签
                    if (aChangeLabelLis[j].getAttribute('name') == _curMarkid) {
                        _thisMarks[0].className = 'selectLabel-ul-labelName curStyle unSelect';
                        _thisMarks[1].className = 'selectLabel-ul-logo curStyle iconfont icon-right';
                    } else {
                        _thisMarks[0].className = 'selectLabel-ul-labelName unSelect';
                        _thisMarks[1].className = 'selectLabel-ul-logo';
                    }
                }
            }
            
            oEvent.cancelBubble = true;  //取消事件冒泡
        };

        document.onclick = function () {
            oAllBookName.style.display = 'none';
            bBookNameShow = false;
            
            oAllLabelName.style.display = 'none';
            bMarkNameShow = false;
        };

        var oNoteBack = document.getElementById('oNoteBack');
        var oUserid = document.getElementsByClassName('userid')[0].getAttribute('name');

        //返回按钮
        oNoteBack.onclick = function () {
            window.location.href = 'main.php';
        }

        var oNoteForm = document.getElementById('oNoteForm');
        var oNoteTextarea = document.getElementsByTagName('textarea')[0];
        var reHTML = /<.*?>+/gi;  //匹配所有html标签

        //完成按钮
        oNoteForm.onsubmit = function () {
            if (oNoteTextarea.value.replace(reHTML, '') == '') {  //除去富文本编辑器在未输入任何文本时默认产生的一些空标签
                layer.msg('笔记里还没写什么东西哦！', {
                    time: 2000 //2s后自动关闭
                    , offset: [100, $(window).width() / 2]
                });

                return false;
            }

            //用户修改原来的笔记，则给url添加noteid表示修改的笔记id，change参数，表示修改
            if (bChangeNote == true && _curNotebookid != '-1') {
                let newURL = './admin/note.notebook.handle.php?noteid=' + oThisNoteid + '&change=true'
                this.setAttribute('action', newURL);  //改变表单提交的地址
            }

            if (_curNotebookid == '-1') {
                if (newNotebookid != '-1' && newMarkid == '-1') {
                    let newURL = './admin/note.notebook.handle.php?notebookid=' + newNotebookid;
                    this.setAttribute('action', newURL);  //改变表单提交的地址
                } else if (newNotebookid == '-1' && newMarkid != '-1') {
                    let newURL = './admin/note.notebook.handle.php?markid=' + newMarkid;
                    this.setAttribute('action', newURL);  //改变表单提交的地址
                } else if (newNotebookid != '-1' && newMarkid != '-1') {
                    let newURL = './admin/note.notebook.handle.php?notebookid=' + newNotebookid + '&markid=' + newMarkid;
                    this.setAttribute('action', newURL);  //改变表单提交的地址
                }
            }
        }
    });  //layui.use(['layer'], function () {  end

    function alertPrompt(obj, urlParam) {
        layer.open({
            type: 1
            , title: false //不显示标题栏
            , closeBtn: false
            , area: ['400px', '250px']
            , shade: 0.6
            , shadeClose: true  //点击遮罩是否关闭
            , id: '19980616' //设定一个id，防止重复弹出
            , btn: ['确定', '取消']
            , btnAlign: 'c'
            , moveType: 1 //拖拽模式，0或者1
            , content: '<div style="height: 180px; line-height: 180px;font-size: 30px;text-align: center;">'
                + '确定要删除吗？'
                + '</div>'
            , success: function () {
                var oGetBtn = document.getElementsByClassName('layui-layer-btn0')[0];
                oGetBtn.onclick = function () {
                    // 点击确认之后的操作
                    ajax('./admin/data.handle.php', urlParam, fnSuccDelete(obj));

                    //重置输入区域
                    oNoteHeader.value = '';
                    editor.txt.clear();
                    editor.txt.html('<p>开始输入你的笔记吧！</p>');
                    $noteText.val(editor.txt.html());
                };
            }
        });
    
        var oNoteSideMainUl = document.getElementById('oNote-side-main-ul');
    
        function fnSuccDelete(obj) {
            oNoteSideMainUl.removeChild(obj);
        }
    }
};  //window.onload  end