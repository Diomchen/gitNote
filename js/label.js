;
window.onload = function () {
    layui.use(['layer'], function () {
        var layer = layui.layer;
        var oMarkBack = document.getElementById('oMarkBack');
        var oUserid = document.getElementsByClassName('userid')[0].getAttribute('name');
        //返回按钮
        oMarkBack.onclick = function () {
            window.location.href = 'main.php';
        };

        var oMarkList = document.getElementById('oMarkList');
        var aMarkLis = oMarkList.getElementsByClassName('mark-li');
        var oPlusMark = document.getElementById('oPlusMark');
        var oSearchInput = document.getElementById('oSearchInput');
        var aMarkListWords = oMarkList.getElementsByClassName('mark-list-word');
        var aNoteOls = oMarkList.getElementsByClassName('note-list');

        showThisBook(oSearchInput.value);

        //搜索框输入（模糊搜索）
        oSearchInput.oninput = function () {
            window.history.pushState(null, null, 'label.php');  //页面无刷新更改url

            showThisBook(this.value);
        };

        //显示搜索到的标签
        function showThisBook(value) {
            for (let i = 0; i < aMarkLis.length; i++) {
                var oNoteHeaderText = aMarkLis[i].getElementsByTagName('h5')[0].innerText;

                if (oNoteHeaderText.match(value)) {  //匹配到的显示
                    aMarkLis[i].style.display = 'block';
                    aNoteOls[i].style.display = 'block';
                } else {
                    aMarkLis[i].style.display = 'none';
                    aNoteOls[i].style.display = 'none';
                }
            }
        }

        oPlusMark.onclick = function () {
            //创建标签的弹出框
            alertPrompt();
        };

        for (let i = 0; i < aMarkLis.length; i++) {
            //判断每个标签下的笔记是否显示
            aNoteOls[i].status = true;

            aMarkListWords[i].onclick = function () {
                if (aNoteOls[i].status == true) {
                    aNoteOls[i].style.display = 'none'
                    aNoteOls[i].status = false;
                } else {
                    aNoteOls[i].style.display = 'block'
                    aNoteOls[i].status = true;
                }
            };
        }

        //显示每个标签中的笔记数目
        var aNoteNumbers = document.getElementsByClassName('noteNumber');
        for (let i = 0; i < aNoteNumbers.length; i++) {
            var aOlLis = aNoteOls[i].getElementsByClassName('note-li');
            aNoteNumbers[i].innerText = aOlLis.length + '条笔记';

            for (let j = 0; j < aOlLis.length; j++) {
                //点击标签中的笔记跳转到笔记界面，并显示相应的笔记
                aOlLis[j].onclick = function () {
                    var _header = this.getElementsByClassName('note-li-header')[0].innerText;
                    var _thisURLParam = 'note.php?input=' + _header;

                    window.location.href = _thisURLParam;
                };
            }
        }

        var aMarkFuncDivs = oMarkList.getElementsByClassName('mark-list-func');
        for (let i = 0; i < aMarkFuncDivs.length; i++) {
            var aMarkFuncSpans = aMarkFuncDivs[i].getElementsByClassName('iconfont');

            for (let j = 0; j < 3; j++) {
                var oUserid = document.getElementsByClassName('userid')[0].getAttribute('name');  //用户id
                var oMarkid = document.getElementsByClassName('mark-li')[i].getAttribute('name');  //标签id
                var urlParamSelectStar = '?userid=' + oUserid + '&id=' + oMarkid + '&table=mark';

                ajax('./admin/data.test.php', urlParamSelectStar, fnSuccStarSelect, fnFailStarSelect);

                aMarkFuncSpans[j].onclick = function () {
                    var _this = this;

                    if (j == 0) {  //分享
                        //........TODO**************

                    } else if (j == 1) {  //收藏
                        var oMarkid = this.parentNode.parentNode.parentNode.getAttribute('name');  //标签id
                        var urlParamHandleStar = '?userid=' + oUserid + '&id=' + oMarkid + '&table=mark&star=true';  //star标志收藏

                        ajax('./admin/data.handle.php', urlParamHandleStar, fnSuccStar, fnFailStar);
                    } else {  //删除
                        var oMarkid = this.parentNode.parentNode.parentNode.getAttribute('name');  //标签id

                        //delete标志删除，deleted标志彻底删除
                        var urlParamHandleDelete = '?userid=' + oUserid + '&id=' + oMarkid + '&table=mark&delete=true&deleted=true';

                        // 确认提示框
                        alertDeletePrompt(this.parentNode.parentNode.parentNode, urlParamHandleDelete);
                    }

                    function fnSuccStar() {
                        _this.className = 'iconfont icon-shoucang iconColorRed';
                    }
                    function fnFailStar() {
                        _this.className = 'iconfont icon-shoucang iconColorGray';
                    }
                };

                function fnSuccStarSelect() {
                    aMarkFuncDivs[i].getElementsByClassName('icon-shoucang')[0].className = 'iconfont icon-shoucang iconColorRed';
                }
                function fnFailStarSelect() {
                    aMarkFuncDivs[i].getElementsByClassName('icon-shoucang')[0].className = 'iconfont icon-shoucang iconColorGray';
                }
            }
        }




    });
};

/**
 * 对用户的提示框
*/
function alertPrompt() {
    //示范一个公告层
    layer.open({
        type: 1
        , title: false //不显示标题栏
        , closeBtn: false
        , area: ['400px', '350px']
        , shade: 0.6
        , shadeClose: true  //点击遮罩是否关闭
        , id: '19980616'  //设定一个id，防止重复弹出
        , btn: ['确定', '取消']
        , btnAlign: 'c'
        , moveType: 1  //拖拽模式，0或者1
        , content: '<div style="height: 250px;">'
            + '<i class="iconfont icon-label" style="display: block;font-size: 70px;width: 400px;height: 150px;line-height: 200px;text-align: center;color: #666;"></i>'
            + '<input id="oMarkName" type="text" placeholder="  请输入标签的名字" '
            + 'style="width: calc(100% - 92px); height: 40px; margin: 40px auto 0 auto;'
            + ' font-size: 30px; color: #a8a8a8; caret-color: #000; padding: 5px 45px; border: 0;border-bottom: 1px solid #ececec;border-top: 1px solid #ececec;" /></div>'
        , success: function (layero) {
            var oMarkName = document.getElementById('oMarkName');
            var oUserid = document.getElementsByClassName('userid')[0];  //用户id

            oMarkName.oninput = function () {
                var btn = layero.find('.layui-layer-btn');
                btn.find('.layui-layer-btn0').attr({
                    href: './admin/data.handle.php?markName=' + this.value
                    , target: '_self'
                });
            }
        }
    });
}

function alertDeletePrompt(obj, urlParam) {
    layer.open({
        type: 1
        , title: false //不显示标题栏
        , closeBtn: false
        , area: ['400px', '250px']
        , shade: 0.6
        , shadeClose: true  //点击遮罩是否关闭
        , id: '19980616' //设定一个id，防止重复弹出
        , btn: ['确定', '取消']
        , btnAlign: 'c'
        , moveType: 1 //拖拽模式，0或者1
        , content: '<div style="height: 180px; line-height: 180px;font-size: 30px;text-align: center;">'
            + '确定要彻底删除吗？'
            + '</div>'
        , success: function () {
            var oGetBtn = document.getElementsByClassName('layui-layer-btn0')[0];
            oGetBtn.onclick = function () {
                // 点击确认之后的操作
                //delete.handle.php 检测用户删除笔记需要用户id和笔记id两个url参数
                ajax('./admin/data.handle.php', urlParam, fnSuccDelete(obj));
            };
        }
    });

    var oNoteSideMainUl = document.getElementById('oMarkList');

    function fnSuccDelete(obj) {
        oNoteSideMainUl.removeChild(obj);
    }
}