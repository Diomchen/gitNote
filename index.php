<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>gitNote</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/form.css">
    <link rel="stylesheet" href="css/iconfont.css">
    <link rel="stylesheet" href="layui/css/layui.css">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/index.js"></script>
    <script src="js/form.js"></script>
    <script src="js/ajax.js"></script>
    <script src="js/iconfont.js"></script>

    <style type="text/css">
        .icon {
            width: 1em; height: 1em;
            vertical-align: -0.15em;
            fill: currentColor;
            overflow: hidden;
        }
    </style>
</head>

<body>
    <!-- layui框架 -->
    <script src="layui/layui.js"></script>

    <main id="main">
        <!-- 遮罩 -->
        <div id="mask"></div>
        
        <!-- 初始登录/注册 -->
        <section id="init">
            <header id="logo">
                <svg class="icon" aria-hidden="true">
                    <use xlink:href="#icon-note"></use>
                </svg>

                <p class="unSelect">gitNote</p>
            </header>
        
            <span id="oInitLogin" class="init-login unSelect borderLine">
                <span></span>
                <p>登录</p>
            </span>
            <span id="oLine" class="init-line"></span>
            <span id="oInitRegister" class="init-register unSelect borderLine">
                <span></span>
                <p>注册</p>
            </span>
        </section>
        
        <section id="corner">
            <div id="oCornerLogin" class="corner-lr corner-login unSelect">
                <div class="corner-lr-word">登录</div>
            </div>
            <div id="oCornerRegister" class="corner-lr corner-register unSelect">
                <div class="corner-lr-word">注册</div>
            </div>
        </section>
        
        <section id="form">
            <form id="oLoginForm" class="login-form" action="./admin/data.handle.php" method="POST">
                <label for="oLoginUser" class="labelWidth">用户名 / 邮箱</label>
                <input id="oLoginUser" class="form-input login-input" type="text" name="loginUserName" placeholder="请输入用户名或邮箱" autocomplete="off" required>
        
                <label for="oLoginPasswd" class="labelWidth">密码</label>
                <input id="oLoginPasswd" class="form-input login-input" type="password" name="loginPasswd" placeholder="请输入密码" autocomplete="off" required>
        
                <div class="form-saveMe">
                    <input id="oSaveMe" type="checkbox" name="checkbox" checked="checked">
                    <label class="unSelect labelWidth" for="oSaveMe">30天内记住我</label>
                </div>
        
                <button id="oLoginBtn" class="from-button unSelect labelWidth" type="submit">登录</button>
            </form>
            
            <form id="oRegisterForm" class="register-form" action="./admin/sendemail.php" method="POST">
                <label for="oRegisterUser" id="oRegUserLabel" class="labelWidth"><p id="oUserExist"></p>用户名</label>
                <input id="oRegisterUser" class="form-input register-input" type="text" name="registerUserName" placeholder="请输入用户名" autocomplete="off" required>

                <label for="oRegisterEmail" id="oRegEmailLabel" class="labelWidth"><p id="oEmailExist"></p>邮箱</label>
                <input value="" id="oRegisterEmail" class="form-input register-input" type="email" name="registerEmail" placeholder="请输入邮箱" autocomplete="off" required>
        
                <label for="oRegisterPasswd" id="oRegPassLabel" class="labelWidth">密码</label>
                <input value="" id="oRegisterPasswd" class="form-input register-input" type="password" name="registerPasswd" placeholder="请输入密码" autocomplete="off" required>
                <br>
                <input id="oRegisterPasswdAgain" class="form-input register-input" type="password" name="registerPasswdAgain" placeholder="请再次输入密码" autocomplete="off" required>
        
                <!-- 密码强度验证进度条 -->
                <div id="oProgressBar" class="register-form-passwdTest">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <!-- 密码强度验证提示文字 -->
                <div class="register-form-testWord">
                    <div>弱</div>
                    <div>中</div>
                    <div>强</div>
                </div>
        
                <button id="oRegisterBtn" class="from-button unSelect" type="submit">注册</button>
            </form>
        </section>
    </main>
</body>

</html>
