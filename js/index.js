;
// 初始界面的特效
$(document).ready(function () {
    var mark = 0;

    $("#oInitLogin").click(function () {
        $('.borderLine span').css({ border: '2px solid #fff' });
        $("#logo").css("display", "none");
        if (mark == 0) {
            $('#oCornerLogin').css('zIndex', -1);
            $("#mask").animate({ height: "100vh" }, 800)  //这里高度不变是用来卡时间
                .animate({ left: "-80px" }, 500);
            $('#oCornerRegister').animate({ zIndex: "0" }, 1300);
            $('#oLine').animate({ height: "100%", top: "0" }, 250)
                .animate({ left: "1400px" }, 250).animate({ height: "0", width: "0", border: "0" }, 0);
            $('#oInitRegister').animate({ fontSize: "0" }, 200)
                .animate({ right: "-600px" }, "slow");
            $(this).animate({ fontSize: "65px" }, 300)
                .animate({ left: "46%" }, 400)
                .animate({ top: "20px", fontSize: "40px" }, 600).css('color', '#79d2bd');
            $('.login-form').css('display', 'block');
            $('.register-form').css('display', 'none');
            $('.login-form').animate({ heigth: "0" }, 1000)  //这里高度也是用来卡时间
                .animate({ top: "200px", height: "auto" }, 300);
            mark++;
        }
    });

    $("#oInitRegister").click(function () {
        $('.borderLine span').css({ border: '2px solid #fff' });
        //初始清空注册表单
        var oRegisterForm = document.getElementById('oRegisterForm');
        var aInputs = oRegisterForm.getElementsByTagName('input');

        for (var i = 0; i < aInputs.length; i++) {
            aInputs[i].value = '';
        }
        
        $("#logo").css("display", "none");
        if (mark == 0) {
            $('#oCornerRegister').css('zIndex', -1);
            $("#mask").animate({ height: "100vh" }, 800)  //这里高度不变是用来卡时间
                .animate({ left: "-900px" }, 500);
            $('#oCornerLogin').animate({ zIndex: "0" }, 1300);
            $('#oLine').animate({ height: "100%", top: "0" }, 250)
                .animate({ left: "-400px" }, 250).animate({ height: "0", width: "0", border: "0" }, 0);
            $('#oInitLogin').animate({ fontSize: "0" }, 200)
                .animate({ left: "-600px" }, "slow");
            $(this).animate({ fontSize: "65px" }, 300)
                .animate({ right: "29%" }, 400)
                .animate({ top: "20px", fontSize: "40px" }, 600).css('color', '#8779d2');
            $('.register-form').css('display', 'block');
            $('.login-form').css('display', 'none');
            $('.register-form').animate({ heigth: "0" }, 1000)  //这里高度也是用来卡时间
                .animate({ top: "200px", height: "auto" }, 300);
            mark++;
        }
    });

    //两个角落
    $("#oCornerLogin").click(function () {
        $(this).css('zIndex', -1);
        $("#mask").animate({ left: "-80px" }, 700);
        $('#oCornerRegister').animate({ zIndex: "0" }, 700);
        $('#oInitRegister').animate({ right: "15%", opacity: "0" });
        $('#oInitLogin').css({ left: '260px', top: '20px', fontSize: "40px", opacity: "0", "color": "#79d2bd" });
        $('#oInitLogin').animate({ left: '46%', opacity: '1' }, 1000);
        $('.register-form').animate({ left: "1000px", width: "0" }, 800)
            .animate({ height: "0" }, 10);
        $('.login-form').css('display', 'block');
        $('.register-form').css('display', 'none');
        $('.login-form').css({ display: 'block', top: "200px", height: "auto", width: "0" });
        $('.login-form').animate({ width: "400px" }, 800);
    });
    $("#oCornerRegister").click(function () {
        //初始清空注册表单
        var oRegisterForm = document.getElementById('oRegisterForm');
        var aInputs = oRegisterForm.getElementsByTagName('input');

        for (var i = 0; i < aInputs.length; i++) {
            aInputs[i].value = '';
            aInputs[i].style.backgroundColor = '#fff';
        }

        $(this).css('zIndex', -1);
        $("#mask").animate({ left: "-900px" }, 700);
        $('#oCornerLogin').animate({ zIndex: "0" }, 700);
        $('#oInitLogin').animate({ left: "330px", opacity: "0" });
        $('#oInitRegister').css({ right: '6%', top: '20px', fontSize: "40px", opacity: "0", "color": "#8779d2" });
        $('#oInitRegister').animate({ right: '29%', opacity: '1' }, 1000);
        $('.login-form').animate({ width: "0" }, 800)
            .animate({ height: "0" }, 10);
        $('.login-form').css('display', 'none');
        $('.register-form').css({ display: 'block', top: "200px", left: '400px', width: "0", height: "auto" });
        $('.register-form').animate({ left: '0', width: "400px" }, 800);
    });
});