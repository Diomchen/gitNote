window.onload = function () {
    var oNoteBack = document.getElementById('oNoteBack');
    //返回按钮
    oNoteBack.onclick = function () {
        window.location.href = 'main.php';
    };

    var oChangeHeadImg = document.getElementById('up-pic');
    // var oChangePass = document.getElementById('changePass-btn');
    var uploadImg = document.getElementById('getHeadImgFile');
    var oPreviewImg = document.getElementById('previewImg');

    oChangeHeadImg.onclick = function () {
        document.querySelector('input[type=file]').click();
    };
    // oChangePass.onclick = function () {
    //     console.log(1);
    // };

    uploadImg.onchange = function () {
        changePic(this);

        var reads = new FileReader();
        var filePath = document.getElementById('getHeadImgFile').files[0];
        var oHeadImg = document.getElementById('oHeadImg');

        reads.readAsDataURL(filePath);
        reads.onload = function () {
            oHeadImg.value = this.result;
        };
    };

    function changePic() {
        var reads = new FileReader();
        var filePath = document.getElementById('getHeadImgFile').files[0];

        reads.readAsDataURL(filePath);
        reads.onload = function (e) {
            oPreviewImg.src = this.result;
        };
    }
};