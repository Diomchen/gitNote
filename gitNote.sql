/*
 Source Server Type    : MySQL
 Source Host           : localhost:3306
 Source Schema         : gitNote
 Target Server Type    : MySQL
 Date: 2018-8-11  13:09:10
*/

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `gitnote`;
CREATE DATABASE gitnote;

USE gitnote;

-- ----------------------------
-- Table structure for mark
-- ----------------------------
DROP TABLE IF EXISTS `mark`;
CREATE TABLE `mark`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `markName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `createTime` timestamp NOT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `isStar` tinyint(1) NOT NULL DEFAULT 0,
  `isDelete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for note
-- ----------------------------
DROP TABLE IF EXISTS `note`;
CREATE TABLE `note`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `markID` int(10) NULL DEFAULT NULL,
  `notebookID` int(10) NULL DEFAULT NULL,
  `header` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `createTime` timestamp NOT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `remindTime` timestamp NULL DEFAULT NULL,
  `isStar` tinyint(1) NOT NULL DEFAULT 0,
  `isDelete` tinyint(1) NOT NULL DEFAULT 0,
  `isShare` tinyint(1) NOT NULL DEFAULT 0,
  `sharedPeople` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for notebook
-- ----------------------------
DROP TABLE IF EXISTS `notebook`;
CREATE TABLE `notebook`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `noteNumber` int(10) NOT NULL DEFAULT 0,
  `bookName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `createTime` timestamp NOT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `isStar` tinyint(1) NOT NULL DEFAULT 0,
  `isShare` tinyint(1) NOT NULL DEFAULT 0,
  `isDelete` tinyint(1) NOT NULL DEFAULT 0,
  `sharedPeople` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `headImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `createTime` timestamp NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token_exptime` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

ALTER TABLE `gitnote`.`mark` 
ADD CONSTRAINT `pk_mark_userid_user_id` FOREIGN KEY (`userid`) REFERENCES `gitnote`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `gitnote`.`note` 
ADD CONSTRAINT `fk_note_markID_id` FOREIGN KEY (`markID`) REFERENCES `gitnote`.`mark` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_note_notebookID_id` FOREIGN KEY (`notebookID`) REFERENCES `gitnote`.`notebook` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_note_userid_user_id` FOREIGN KEY (`userid`) REFERENCES `gitnote`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `gitnote`.`notebook` 
ADD CONSTRAINT `fk_notebook_userid_user_id` FOREIGN KEY (`userid`) REFERENCES `gitnote`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;