<?php
    session_start();
    require_once('./admin/connect.php');

    //用户注销
    if (!isset($_SESSION['userid'])) {
        echo "<script>alert('您已注销，请重新登录！'); location.href='index.php'</script>";
        exit;
    }

    //用户还未激活
    if (isset($_SESSION['userid'])) {
        $testActive = "select * from user where id=".$_SESSION['userid'];
        $res = mysqli_query($con, $testActive);
        if (mysqli_fetch_assoc($res)['status'] == '0') {
            echo "<script>alert('用户还未激活，请前往邮箱激活！'); location.href='index.php'</script>";
            exit;
        }
    }
    
    $userid = $_SESSION['userid'];

    if (isset($_GET['input'])) {
        $input = $_GET['input'];
    }

    //查询note中的所有信息
    $sql = "select * from note where userid=$userid order by updateTime desc";
    $result = mysqli_query($con, $sql);

    if ($result && mysqli_num_rows($result)) {
        while($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }
    } else {
        $data = array();
    }

    //查询notebook中的所有信息
    $notebookSelectSql = "select * from notebook where userid=$userid order by updateTime desc";
    $notebookSelectResult = mysqli_query($con, $notebookSelectSql);

    if ($notebookSelectResult && mysqli_num_rows($notebookSelectResult)) {
        while($notebookRow = mysqli_fetch_assoc($notebookSelectResult)) {
            $notebookData[] = $notebookRow;
        }
    } else {
        $notebookData = array();
    }

    //查询mark中的所有信息
    $labelSelectSql = "select * from mark where userid=$userid order by updateTime desc";
    $labelSelectResult = mysqli_query($con, $labelSelectSql);

    if ($labelSelectResult && mysqli_num_rows($labelSelectResult)) {
        while($labelRow = mysqli_fetch_assoc($labelSelectResult)) {
            $labelData[] = $labelRow;
        }
    } else {
        $labelData = array();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>gitNote</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/iconfont.css">
    <link rel="stylesheet" href="layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="css/note.css">
    <script src="js/ajax.js"></script>
</head>
<body>
    <!-- layui框架 -->
    <script src="layui/layui.js"></script>
    
    <!-- 笔记区域 -->
    <div id="note" class="main-subSection layui-row">
        <div id="oNoteBack" class="unSelect">返回</div>
        
        <!-- 笔记页面左边栏 -->
        <section id="note-side" class="layui-hide-xs layui-hide-sm layui-show-md-block">
            <header class="note-side-header">
                <div class="note-side-top">
                    <p>笔记</p>

                    <button id="oPlusNote"><i class="iconfont icon-add"></i></button>
                </div>

                <div class="note-search">
                    <div class="note-search-div">
                        <input id="oSearchInput" class="note-search-input" type="text" placeholder="搜索笔记内容或标题" value="<?php 
                            if (isset($input)) {
                                echo $input;
                            } else {
                                echo '';
                            }
                        ?>"/>
                        <img class="note-search-logo" src="imgs/search.png" alt="">
                    </div>
                </div>
            </header>

            <main class="note-side-main">
                <!-- 隐藏域 -->
                <div class="userid" name="<?php echo $userid?>" style="display: none;"></div>
                
                <ul id="oNote-side-main-ul">
                    <?php
                        if(!empty($data)) {
                            rsort($data);  //对数组排序

                            foreach($data as $value) {
                                if ($value['isDelete'] == 0) {
                    ?>

                    <!-- 笔记略缩图区域 -->
                    <li name="<?php echo $value['id']?>" class="note-side-main-li">
                        <h5><?php echo $value['header']?></h5>
                        <div class="note-side-main-func">
                            <span class="iconfont icon-clock iconColorGray" name="<?php echo $value['remindTime']?>"></span>
                            <span class="iconfont icon-shoucang iconColorGray" name="<?php echo $value['isStar']?>"></span>
                            <span class="iconfont icon-delete iconColorGray" name="<?php echo $value['isDelete']?>"></span>

                            <!-- 隐藏域 -->
                            <div class="markID" name="<?php echo $value['markID']?>" style="display: none;"></div>
                            <div class="notebookID" name="<?php echo $value['notebookID']?>" style="display: none;"></div>
                            <div name="<?php echo $value['isShare']?>" style="display: none;"></div>
                            <div name="<?php echo $value['sharedPeople']?>" style="display: none;"></div>
                        </div>

                        <main><p><?php echo emoji_decode($value['content'])?></p></main>
                        
                        <i><?php echo $value['createTime']?></i>
                        <hr>
                    </li>

                    <?php
                                }
                    
                            }
                                
                        }

                        //对emoji表情转反义
                        function emoji_decode($str){
                            $strDecode = preg_replace_callback('|\[\[EMOJI:(.*?)\]\]|', function($matches){  
                                return rawurldecode($matches[1]);
                            }, $str);

                            return $strDecode;
                        }
                    ?>
                </ul>
            </main>
        </section>

        <!-- 笔记页面编辑栏 -->
        <section id="note-main">
            <header>
                <div id="oChangeNotebook" class="note-main-change">
                    <div class="note-main-change-pos">
                        <div id="oShowBookName" class="note-main-changeBook">
                            <span class="iconfont icon-notebook"></span><!--
                         --><span class="unSelect">笔记本</span>
                        </div>

                        <div id="oShowLabelName" class="note-main-changeLabel">
                            <span class="iconfont icon-label"></span><!--
                         --><span class="unSelect note-main-changeLabel-label">标签</span>
                        </div>
                    </div>

                    <!-- 笔记本列表 -->
                    <div id="oAllBookName" class="selectNotebook">
                        <ul id="oSelectBookUl" class="selectNotebook-ul">
                            <?php
                                if(!empty($notebookData)) {
                                    rsort($notebookData);

                                    foreach($notebookData as $notebookValue) {
                            ?>

                            <li class="selectNotebook-ul-li" name="<?php echo $notebookValue['id']?>">
                                <span class="selectNotebook-ul-bookName unSelect"><?php echo $notebookValue['bookName']?></span>
                                <span class="selectNotebook-ul-logo"></span>
                                <hr>
                            </li>

                            <?php
                                    }
                                }
                            ?>
                        </ul>
                    </div>

                    <!-- 标签列表 -->
                    <div id="oAllLabelName" class="selectLabel">
                        <ul id="oSelectLabelUl" class="selectLabel-ul">
                            <?php
                                if(!empty($labelData)) {
                                    rsort($labelData);

                                    foreach($labelData as $labelValue) {
                            ?>

                            <li class="selectLabel-ul-li" name="<?php echo $labelValue['id']?>">
                                <span class="selectLabel-ul-labelName unSelect"><?php echo $labelValue['markName']?></span>
                                <span class="selectLabel-ul-logo"></span>
                                <hr>
                            </li>

                            <?php
                                    }
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </header>
            
            <!-- 笔记输入区域 -->
            <footer class="note-main-footer">
                <!-- 文本编辑功能选项 -->
                <nav>
                    <!-- 编辑工具栏 -->
                    <div id="note-main-footer-toolbar" class="note-toolbar"></div>
                </nav>

                <!-- 遮住文本溢出时的竖直滚动条 -->
                <div id="oHiddenScroll" class="hidden-scroll-y">
                    <form id="oNoteForm" action="./admin/note.notebook.handle.php" method="POST">
                        <!-- 中间的隔离带（标题输入框） -->
                        <input id="oNoteHeader" type="text" name="header" placeholder="在这里写下笔记的标题">

                        <!-- 隐藏域 -->
                        <textarea id="note-text" name="content" cols="30" rows="10"></textarea>

                        <button type="submit" class="note-main-complete unSelect">完成</button>
                    </form>

                    <div id="note-main-footer-editor" class="note-editor"><p>开始记下你的笔记吧！</p></div>
                </div>

            </footer>
        </section>
    </div>

    <script src="js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="js/wangEditor.js"></script>
    <script type="text/javascript" src="js/editor.js"></script>
    <script type="text/javascript" src="js/expression.js"></script>
</body>
</html>