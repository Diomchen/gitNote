;
window.onload = function () {
    layui.use(['layer'], function () {
        var layer = layui.layer;
        var oNoteBack = document.getElementById('oNoteBack');
        var oUserid = document.getElementsByClassName('userid')[0].getAttribute('name');

        //返回按钮
        oNoteBack.onclick = function () {
            window.location.href = 'main.php';
        };

        //回收站里有东西进行操作
        if (document.getElementsByClassName('oDeleted')[0] && document.getElementsByClassName('oRestore')[0]) {
            var oBinMainUl = document.getElementById('oBin-main-ul');
            var aBinMainUlLis = oBinMainUl.getElementsByClassName('bin-main-li');
            var iNoteLiLength = aBinMainUlLis.length;

            for (let i = 0; i < iNoteLiLength; i++) {
                var aRestores = oBinMainUl.getElementsByClassName('oRestore');
                var aDeleteds = oBinMainUl.getElementsByClassName('oDeleted');

                //还原
                aRestores[i].onclick = function () {
                    //将要还原的笔记的id拼接到url参数里
                    var _thisNoteid = this.parentNode.parentNode.getAttribute('name');
                    var thisUrlParam = '?userid=' + oUserid + '&id=' + _thisNoteid + '&table=note&delete=true';

                    ajax('./admin/data.handle.php', thisUrlParam, fnDeleteNote(this));
                };

                //彻底删除
                aDeleteds[i].onclick = function () {
                    var _thisNoteid = this.parentNode.parentNode.getAttribute('name');
                    var urlParamDeleted = '?userid=' + oUserid + '&id=' + _thisNoteid + '&table=note&delete=true&deleted=true';

                    ajax('./admin/data.handle.php', urlParamDeleted, fnDeleteNote(this));
                };

                //将笔记从回收站中移除
                function fnDeleteNote(obj) {
                    oBinMainUl.removeChild(obj.parentNode.parentNode);
                }

            }  // for (let i = 0; i < iNoteLiLength; i++) {  end

        }

    });
};