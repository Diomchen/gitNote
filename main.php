<?php
    session_start();
    require_once('./admin/connect.php');

    //用户注销
    if (!isset($_SESSION['userid'])) {  //用户未登录
        echo "<script>alert('您已注销，请重新登录！'); location.href='index.php'</script>";
        exit;
    }

    //用户还未激活
    if (isset($_SESSION['userid'])) {  //用户登录
        $testActive = "select * from user where id=".$_SESSION['userid'];
        $res = mysqli_query($con, $testActive);
        if (mysqli_fetch_assoc($res)['status'] == '0') {
            echo "<script>alert('用户还未激活，请前往邮箱激活！'); location.href='index.php'</script>";
            exit;
        } else if($_SESSION['login'] == '0') {
            echo "<script>alert('登录成功！');</script>";
            $_SESSION['login'] = '1';
        }
    }

    $userid = $_SESSION['userid'];
    $userName = $_SESSION['userName'];

    //查询用户表
    $userSql = "select * from user where id=$userid";
    $userRes = mysqli_query($con, $userSql);
    $userRow = mysqli_fetch_assoc($userRes);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>gitNote</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="layui/css/layui.css">
    <link rel="stylesheet" href="css/iconfont.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/main.js"></script>
</head>

<body>
    <!-- layui框架 -->
    <script src="layui/layui.js"></script>

    <!-- 搜索及挂件区 -->
    <div id="other-func" class="layui-row unSelect">
        <div id="other-func-user" class="layui-col-xs12 layui-col-sm4 layui-col-md4">
            <div class="other-func-user">
                <span class="user">
                    <?php
                        if ($userRow['headImg']) {
                            echo "<img src=\"".$userRow['headImg']."\" alt=\"user\">";
                        } else {
                            echo "<i class=\"iconfont icon-yonghu\"></i>";
                        }
                    ?>
                </span>
                <p><?php echo $userName;?>, 欢迎！</p>

                <div class="user-option">
                    <div id="user-option-mine"><i class="iconfont icon-yonghu2"></i>个人中心</div>
                    <hr>
                    <div id="user-optoin-logout"><i class="iconfont icon-logout"></i>退出登录</div>
                </div>
            </div>
        </div>

        <div id="other-func-time" class="layui-hide-xs layui-col-sm4 layui-col-md4">
            <div>
                <span>HH</span>
                <span>:</span>
                <span>MM</span>
            </div>

            <div>
                <span>YYYY</span>
                <span>/</span>
                <span>MM</span>
                <span>/</span>
                <span>DD</span>
            </div>
        </div>
        <div id="other-func-share" class="layui-hide-xs layui-col-sm4 layui-col-md4">
            <div class="other-func-share-div">
                <i class="iconfont icon-fenxiang"></i>
            </div>

            <div class="func-share-list">
                <!-- 分享 -->
            </div>
        </div>
    </div>

    <!-- 彩块区 -->
    <div id="color">
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
    </div>

    <!-- 垃圾桶背景块 -->
    <div id="color-Bin">
        <div class="re-block"></div>
    </div>

    <!-- 功能切换专区 -->
    <div id="main-box">
        <div id="class-strip" class="unSelect">
            <!-- 以下是各种长方形分类条，主体在条内 -->
            <div class="strip" style="width:19%">
                <!-- 
                        分为三大块  1.空白块（卡长度,又长度不同）  
                                2.图片块
                                3.标签块 
                    -->
                <h3 class="strip-num" style="top:62vh;left:14vw">01</h3>
                <span class="strip-sign" style="top:64vh;left:16vw">标签</span>
                <div class="overide">
                    <div class="w-box" style="height:66%"></div>
                    <a href="javascript:;" onclick="delayJump('label.php');"><img class="strip-img" src="imgs/t1.jpg" alt=""></a>
                </div>
            </div>

            <div class="strip" style="width:20%;margin-left:2%">
                <h3 class="strip-num" style="top:45vh;left:36vw">02</h3>
                <span class="strip-sign" style="top:48vh;left:38vw">笔记</span>
                <div class="overide">
                    <div class="w-box" style="height:49%"></div>
                    <a href="javascript:;" onclick="delayJump('note.php');"><img class="strip-img" src="imgs/t2.jpg" alt=""></a>
                </div>
            </div>

            <div class="strip" style="width:19%;margin-left:2%">
                <h3 class="strip-num" style="top:59vh;left:57vw">03</h3>
                <span class="strip-sign" style="top:64vh;left:58.3vw">笔记本</span>
                <div class="overide">
                    <div class="w-box" style="height:63%"></div>
                    <a href="javascript:;" onclick="delayJump('notebook.php?num=0');"><img class="strip-img" src="imgs/t3.jpg" alt=""></a>
                </div>
            </div>

            <div class="strip" style="width:24%;margin-left:2%">
                <h3 class="strip-num" style="top:55vh;left:83vw">04</h3>
                <span class="strip-sign" style="top:58vh;left:84.3vw">收藏</span>
                <div class="overide">
                    <div class="w-box" style="height:59%"></div>
                    <a href="javascript:;" onclick="delayJump('mark.php');"><img class="strip-img" src="imgs/t4.jpg" alt=""></a>
                </div>
            </div>

            <div class="strip" style="width:12%;">
                <h3 class="strip-num ext-num" style="top:66.4vh;left:92.5vw">05</h3>
                <span class="strip-sign ext-sign" style="top:75vh;left:91vw;color:#ffffff;">回收站</span>
                <span class="back">Back</span>
                <div class="overide ">
                    <div class="w-box r-box" style="height:70%"></div>
                    <a href="javascript:;" onclick="delayJump('bin.php');"><div class="Bin"></div></a>
                </div>
            </div>
        </div>
    </div>

    <!-- 背景大图片区 -->
    <div id="picture">
        <img class="all-pic" src="imgs/1.jpg" alt="">
        <img class="all-pic" src="imgs/2.jpg" alt="">
        <img class="all-pic" src="imgs/3.jpg" alt="">
        <img class="all-pic" src="imgs/4.jpg" alt="">
    </div>

    <script>
        function delayJump(url) {  //延迟跳转
            setTimeout('window.location.href="' + url + '"', 1500);
        }
        
        //用户注销
        $(document).ready(function () {
            $("#user-optoin-logout").click(function () {
                $.ajax({
                    url: "./admin/data.handle.php?logout=true",
                    type: 'GET',
                    success: function () {
                        $(location).attr('href', 'index.php');
                    }
                });
            });
        });
    </script>
</body>

</html>