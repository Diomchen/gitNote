<?php
    session_start();
    require_once('./admin/connect.php');

    //用户注销
    if (!isset($_SESSION['userid'])) {
        echo "<script>alert('您已注销，请重新登录！'); location.href='index.php'</script>";
        exit;
    }

    //用户还未激活
    if (isset($_SESSION['userid'])) {
        $testActive = "select * from user where id=".$_SESSION['userid'];
        $res = mysqli_query($con, $testActive);
        if (mysqli_fetch_assoc($res)['status'] == '0') {
            echo "<script>alert('用户还未激活，请前往邮箱激活！'); location.href='index.php'</script>";
            exit;
        }
    }
    
    $userid = $_SESSION['userid'];

    if (isset($_GET['input'])) {
        $input = $_GET['input'];
    }

    $selectNotebook = "select * from notebook where userid=$userid";
    $notebookResult = mysqli_query($con, $selectNotebook);

    //将每个笔记本存入数组
    if ($notebookResult && mysqli_num_rows($notebookResult)) {
        while($notebookRow = mysqli_fetch_assoc($notebookResult)) {
            $dataNotebook[] = $notebookRow;
        }
    } else {
        $dataNotebook = array();
    }

    //更新笔记本中的笔记数
    $selectNoteNumber = "select notebookID,count(*) noteNumber
                         from note
                         where userid=$userid
                         group by notebookID";
    $selectNoteNumberRes = mysqli_query($con, $selectNoteNumber);

    while($noteRow = mysqli_fetch_assoc($selectNoteNumberRes)) {
        $updateNotebook = "update notebook set noteNumber=". $noteRow['noteNumber'] ." where userid=$userid and id=".$noteRow['notebookID'];
        mysqli_query($con, $updateNotebook);
    }

    $selectNote = "select * from note where userid=$userid";
    $selectNoteResult = mysqli_query($con, $selectNote);

    //将每个笔记本中的笔记存入数组
    if ($selectNoteResult && mysqli_num_rows($selectNoteResult)) {
        while($allNoteRow = mysqli_fetch_assoc($selectNoteResult)) {
            $dataNote[] = $allNoteRow;
        }
    } else {
        $dataNote = array();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>gitNote</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/iconfont.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/notebook.css">
    <script type="text/javascript" src="js/notebook.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
</head>
<body>
    <!-- layui框架 -->
    <script src="layui/layui.js"></script>

    <script>
        //进入这个页面刷新一次(用于更新笔记本中的笔记数)
        var curURL = location.href;

        if (curURL.match('num=0')) {
            var newURL = curURL.replace(/num=0/, 'num=1');
            self.location.href = newURL;
        }
    </script>

    <aside>
        <div id="oNoteBack" class="unSelect">返回</div>
    </aside>

    <section id="notebook">
        <header  class="notebook-header">
            <div class="notebook-header-word">
                <p>笔记本</p>
                
                <button id="oPlusNotebook"><i class="iconfont icon-add"></i></button>
            </div>

            <div class="search-div">
                <input id="oSearchInput" class="search-input" type="type" placeholder="搜索笔记本标题" value="<?php 
                    if (isset($input)) {
                        echo $input;
                    } else {
                        echo '';
                    }
                ?>"/>
                <img class="search-logo" src="imgs/search.png" alt="">
            </div>
        </header>

        <main class="notebook-main">
            <!-- 隐藏域 -->
            <div class="userid" name="<?php echo $userid?>" style="display: none;"></div>
            
            <!-- 笔记本 -->
            <ul id="oNoteBookList" class="notebook-list">
                <?php
                    if (!empty($dataNotebook)) {
                        sort($dataNotebook);
                        
                        foreach($dataNotebook as $valueNotebook) {
                            if ($valueNotebook['isDelete'] == 0) {
                ?>
                
                <li class="notebook-li" name="<?php echo $valueNotebook['id']?>">
                    <div class="notebook-li-top">
                        <div class="notebook-list-word">
                            <h5 class="unSelect"><i class="iconfont icon-notebook"></i><?php echo $valueNotebook['bookName']?></h5>
                            <p class="noteNumber unSelect">xx条笔记</p>
                        </div>

                        <div class="notebook-list-func">
                            <span class="iconfont icon-fenxiang"></span>
                            <span class="iconfont icon-shoucang"></span>

                            <!-- 默认笔记本不显示删除按钮 -->
                            <?php
                                if ($valueNotebook['bookName'] == 'default') {
                            ?>
                            <span class="iconfont icon-delete" style="display: none;"></span>
                            <?php
                                } else {
                            ?>
                            <span class="iconfont icon-delete"></span>
                            <?php
                                }
                            ?>
                        </div>
                    </div>

                    <ol class="note-list">
                        <?php
                            if (!empty($dataNote)) {
                                rsort($dataNote);

                                foreach($dataNote as $valueNote) {
                                    if ($valueNote['isDelete'] == 0 && $valueNote['notebookID'] == $valueNotebook['id']) {                                    
                        ?>

                        <li class="note-li" name="<?php echo $valueNote['id']?>">
                            <h6 class="note-li-header"><?php echo $valueNote['header']?></h6>
                            <main><p><?php echo emoji_decode($valueNote['content']);?></p></main>
                            <i><?php echo $valueNote['createTime']?></i>
                        </li>

                        <?php
                                    }
                                }
                            }
                        ?>
                    </ol>
                </li>

                <?php
                            }
                        }
                    }

                    //对emoji表情转反义
                    function emoji_decode($str){
                        $strDecode = preg_replace_callback('|\[\[EMOJI:(.*?)\]\]|', function($matches){  
                            return rawurldecode($matches[1]);
                        }, $str);

                        return $strDecode;
                    }
                ?>
            </ul>
        </main>
    </section>
</body>
</html>