<?php
    session_start();
    require_once('./admin/connect.php');

    //用户注销
    if (!isset($_SESSION['userid'])) {
        echo "<script>alert('您已注销，请重新登录！'); location.href='index.php'</script>";
        exit;
    }

    //用户还未激活
    if (isset($_SESSION['userid'])) {
        $testActive = "select * from user where id=".$_SESSION['userid'];
        $res = mysqli_query($con, $testActive);
        if (mysqli_fetch_assoc($res)['status'] == '0') {
            echo "<script>alert('用户还未激活，请前往邮箱激活！'); location.href='index.php'</script>";
            exit;
        }
    }
    
    $userid = $_SESSION['userid'];

    $noteSql = "select * from note where userid=$userid order by updateTime desc";
    $noteResult = mysqli_query($con, $noteSql);

    if ($noteResult && mysqli_num_rows($noteResult)) {
        while($noteRow = mysqli_fetch_assoc($noteResult)) {
            $noteData[] = $noteRow;
        }
    } else {
        $noteData = array();
    }

    $notebookSql = "select * from notebook where userid=$userid order by updateTime desc";
    $notebookResult = mysqli_query($con, $notebookSql);

    if ($notebookResult && mysqli_num_rows($notebookResult)) {
        while($notebookRow = mysqli_fetch_assoc($notebookResult)) {
            $notebookData[] = $notebookRow;
        }
    } else {
        $notebookData = array();
    }

    $markSql = "select * from mark where userid=$userid order by updateTime desc";
    $markResult = mysqli_query($con, $markSql);

    if ($markResult && mysqli_num_rows($markResult)) {
        while($markRow = mysqli_fetch_assoc($markResult)) {
            $markData[] = $markRow;
        }
    } else {
        $markData = array();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>gitNote</title>
    <link rel="shortcut icon" href="./imgs/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/iconfont.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/mark.css">
    <script type="text/javascript" src="js/mark.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
</head>
<body>
    <!-- layui框架 -->
    <script src="layui/layui.js"></script>
    
    <aside>
        <div id="oNoteBack" class="unSelect">返回</div>
    </aside>
    
    <section id="mark" class="filter-blur">
        <header  class="mark-header">
            <p>快捷方式</p>
        </header>

        <main class="mark-main">
            <!-- 隐藏域 -->
            <div class="userid" name="<?php echo $userid?>" style="display: none;"></div>
            
            <!-- 笔记本 -->
            <ul id="oMarkList" class="mark-list">
                <?php
                    if (!empty($notebookData)) {
                        sort($notebookData);
                        
                        foreach($notebookData as $notebookValue) {
                            if ($notebookValue['isStar'] == 1 && $notebookValue['isDelete'] == 0) {
                ?>
                <li class="mark-list-li" name="<?php echo $notebookValue['id']?>">
                    <i class="iconfont icon-notebook" name="notebook"></i>
                    <h6 class="unSelect"><?php echo $notebookValue['bookName']?></h6>
                    <span class="iconfont icon-jianhao"></span>
                </li>
                <?php
                            }
                        }
                    }
                ?>
                
                <?php
                    if (!empty($noteData)) {
                        rsort($noteData);

                        foreach($noteData as $noteValue) {
                            if ($noteValue['isStar'] == 1 && $noteValue['isDelete'] == 0) {                                    
                ?>
                <li class="mark-list-li" name="<?php echo $noteValue['id']?>">
                    <i class="iconfont icon-note" name="note"></i>
                    <h6 class="unSelect"><?php echo $noteValue['header']?></h6>
                    <span class="iconfont icon-jianhao"></span>
                </li>
                <?php
                            }
                        }
                    }
                ?>

                <?php
                    if (!empty($markData)) {
                        sort($markData);

                        foreach($markData as $markValue) {
                            if ($markValue['isStar'] == 1 && $markValue['isDelete'] == 0) {
                ?>
                <li class="mark-list-li" name="<?php echo $markValue['id']?>">
                    <i class="iconfont icon-label" name="label"></i>
                    <h6 class="unSelect"><?php echo $markValue['markName']?></h6>
                    <span class="iconfont icon-jianhao"></span>
                </li>
                <?php
                            }
                        }
                    }
                ?>
            </ul>
        </main>
    </section>
</body>
</html>